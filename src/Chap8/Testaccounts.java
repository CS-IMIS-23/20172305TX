package Chap8;

public class Testaccounts {
    public static void main(String[] args) {
        Account2 acc = new Account2();

        acc.addAccount("郭恺", 20172301, 100);//添加账户
        acc.addAccount("侯泽洋", 20172302, 2000);//添加账户
        acc.addAccount("范雯琪",20172303,0);//添加账户
        acc.setAccount("段志轩",20172304);//创建账户
        acc.setAccount("谭鑫",20172305);//创建账户
        acc.getDrawMoney("郭恺",20172301,6000);//郭恺的账户进行存钱
        acc.getDraw("郭恺",20172301,4000);//郭恺的账户进行取钱
        System.out.println(acc);
        acc.addaccrual();
        System.out.println("加过利息之后: ");
        System.out.println(acc);
    }
}
