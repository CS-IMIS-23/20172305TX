package Chap8;
//*************************************************************************************
//  Account2.java           Author: sanjin
//
//  创建一个数组进行管理不超过30个的银行账户，并对每一个账户设置一定的方法。
//*************************************************************************************
public class Account2 {
  private Account1[] accounts;
  private int a;
  public Account2()//创造一个最大能管理30个用户的数组
  {
      accounts = new Account1[30];
      a = 0;
  }
  //在数组内进行添加账户
  public void addAccount(String owner, long account, double initial)
  {
      if (a < accounts.length)
      {
          accounts[a] = new Account1(owner, account, initial);
          a++;
      }
  }
  //在数组内进行创建账户
  public void setAccount(String owner, long account)
  {
      accounts[a] = new Account1(owner, account,0);
      a++;
  }
  //往账户里面进行存款，通过判断户名和帐号是否一致来进行
  public void getDrawMoney(String owner, long account, double initial)
  {
      for(int u = 0;u < 30;u++)
         if(accounts[u] != null)
           if(accounts[u].getName().equals(owner)&&accounts[u].getNumber() == account)
               accounts[u].deposit(initial);
  }
  //从账户里面进行取款，通过判断户名和帐号是否一致来进行，并索取银行工作费用
  public void getDraw(String owner, long account, double initial)
  {
    for (int n = 0; n < 30;n++)
        if (accounts[n] != null)
            if(accounts[n].getName().equals(owner)&&accounts[n].getNumber() == account)
              if (accounts[n].getNumber() > initial)
                 accounts[n].withdraw(initial);
  }
  //对银行账户内的存款进行加利息运算
  public void addaccrual()
  {
      int b = 0;
      while(b < accounts.length)
      {
          if (accounts[b] != null)
          {
              accounts[b].addInterest();
          }
          b++;
      }
  }
  public String toString()
  {
      String result = "--------------------------------------\n";

      for (int d = 0; d < 30; d++)
      {
          if(accounts[d] != null)
          result += accounts[d].toString() + "\n\n";
      }
      return result;
  }
}

