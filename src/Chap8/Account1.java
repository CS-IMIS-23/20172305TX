package Chap8;
//*************************************************************************************
//  Account1.java           Author:sanjin
//
//  针对每一个账户内的帐号、户名、本金进行的一些方法。
//*************************************************************************************
import java.text.NumberFormat;
public class Account1 {
    private long acctNumber;
    private double balance,RATE = 0.03;
    private String name,result;
    public Account1(String owner, long account, double initial)
    {
        name = owner;
        acctNumber = account;
        balance = initial;
    }
    //访问账户的户名
    public String getName()
    {
        return name;
    }
    //访问账户的帐号
    public double getNumber()
    {
        return acctNumber;
    }
    //储蓄，进行存款
    public double deposit(double amount)
    {
        balance = balance + amount;
        return balance;
    }
    // 取钱，并扣除小费
    public double withdraw(double amount)
    {
        balance = balance - amount - 1.5;
        return balance;
    }
    //银行支付利息后，账户上的存款
    public double addInterest()
    {
        balance += (balance * RATE);
        return balance;
    }
    public String toString()
    {
        NumberFormat fmt = NumberFormat.getCurrencyInstance();
        if (balance <= 0)
            result = "You account: " + name + "\nYou accNumber: " + acctNumber + "\nYour account assets are zero and on the verge of freezing.";
        else
            result = "You account: " + name + "\nYou accNumber: " + acctNumber + "\nYou account is in normal condition.\nYou corpus :" + balance;
        return result;
    }
}
