//*************************************************************************************
//  computeAverage.java		Author: sanjin
//
//  Count the average number of grades under a student ID. 
//*************************************************************************************
public class computeAverage
{
  public static void main(String ... args)
  {
    double average = 0.0;
    int sum = 0;
     
    if (args.length != 0)
     {for (int i = 1; i <args.length; i++)
        {sum += Integer.parseInt(args[i]);}
      average = sum / (args.length - 1);
      System.out.println(args[0] + "'s average score is:" + average);}
    else
      System.out.println("Enter a student ID and achievements in various subjects again");
  }
}     
   
