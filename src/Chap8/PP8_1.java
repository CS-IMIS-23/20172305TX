package Chap8;
//*************************************************************************************
//  PP8_1.java              Author: sanjin
//
//  Calculate the number of occurrences of each output data.
//*************************************************************************************
import java.util.Scanner;
public class PP8_1 {

    public static void main(String[] args){
     Scanner scan = new Scanner(System.in);
     int value;
     int[] number = new int[51];

     // sentinel value of less-than 0 or greater than 50 to terminate loop
     System.out.print("Enter an integer (more than 50 to quit): ");
     value = scan.nextInt();
     number[value] += 1;

     while (value < 50 && value > 0) {
         System.out.print("Enter an integer (more than 50 to quit): ");
         value = scan.nextInt();
         if (value > 50)
             break;
         number[value] += 1;
     }
     // Represents the number occurrences of 0 ~ 50
     for (int a = 0; a <=50; a++)
       System.out.println(a + ": " + number[a]);
    }
 }

