package Chap8;
//*************************************************************************************
//  PP8_5.java          Author: sanjin
//
//  Calculation of mwan value and Standard Variance based on Floating-point number.
//*************************************************************************************
import java.util.Random;
public class PP8_5 {
    public static void main(String[] args){
    Random generator = new Random();
    int b, d, mean1 = 0;
    double sd1 = 0, sd, mean;
    int f = 0;
    d = generator.nextInt(50);

    int[] number = new int[d];
    double[] sdNum = new double[d];
    // 随机输出数组的长度、利用随机数确定数组的每个元素。
    for (int a = 0; a < d; a++) {
        b = generator.nextInt(10000);
        number[a] = b;
        mean1 += number[a];
      }
    // 输出随机确定的数组的数，并每6个进行分行。
    System.out.println("计算" + d + "个随机数的平均数和均值以下为各个随机数:");

    for (int value : number) {
        System.out.print(value + "\t");
        if (d % 6 == 0)
            System.out.println();
    }
    // 计算平均数(期望)
    System.out.println();
    mean = (double)mean1 / d;
    System.out.println("mean value：" + mean);
    // 计算方差
    for (int e = 0; e < d; e++)
      { sdNum[e] = Math.pow(number[e] - mean, 2);
        sd1 += sdNum[f];
        f++;
      }

    sd = Math.sqrt(sd1);
    System.out.println("Standard variance: " + sd);
    }
}
