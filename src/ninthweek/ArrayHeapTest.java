package ninthweek;

public class ArrayHeapTest {
    public static void main(String args[]){
        ArrayHeap arrayHeap = new ArrayHeap();
        arrayHeap.addElement("23");
        arrayHeap.addElement("54");
        arrayHeap.addElement("78");
        arrayHeap.addElement("2");
        arrayHeap.addElement("9");
        arrayHeap.addElement("7");

        System.out.println(arrayHeap.findMin());
        System.out.println(arrayHeap.removeMin());
        System.out.println(arrayHeap.toString());
    }
}
