package ninthweek;

import java.util.Iterator;

public class LinkedHeapTest {
    public static void main(String[] args) {
        LinkedHeap linkedHeap = new LinkedHeap();
        linkedHeap.addElement("23");
        linkedHeap.addElement("54");
        linkedHeap.addElement("78");
        linkedHeap.addElement("90");
        Iterator postTree = linkedHeap.iteratorPostOrder();
        String result = "";
        while(postTree.hasNext()){
            result += postTree.next() + " ";
        }
        System.out.println(result);
    }
}
