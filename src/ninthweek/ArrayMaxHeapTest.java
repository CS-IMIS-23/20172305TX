package ninthweek;

public class ArrayMaxHeapTest {
    public static void main(String[] args) {
        ArrayMaxHeap arrayHeap = new ArrayMaxHeap();
        int[] ints1 = {36,30,18,40,32,45,22,50};
        Object[] ints = new Object[ints1.length];
        for(int a = 0; a < ints1.length; a++){
            arrayHeap.addElement(ints1[a]);
        }

        System.out.println("大顶堆的层序输出：" + arrayHeap.toString());

        for(int n = 0; n < ints1.length; n++){
            ints[n] = arrayHeap.removeMax();
            String result = "";
            for(int a = 0; a <= n; a++){
                result += ints[a] + " ";
            }
            System.out.println("第" + (n+1) + "次：" + arrayHeap.toString() + "| " + result);
        }

        System.out.print("排序结果： ");
        String result = "";
        for(int m = 0; m < ints1.length; m++){
            result += ints[m] + " ";
        }
        System.out.println(result);
    }
}
