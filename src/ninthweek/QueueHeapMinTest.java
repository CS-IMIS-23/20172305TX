package ninthweek;

public class QueueHeapMinTest {
    public static void main(String[] args) {
        QueueHeapMin queueHeapMin = new QueueHeapMin();
        QueueHeap queueHeap0 = new QueueHeap("10");
        QueueHeap queueHeap1 = new QueueHeap("8");
        QueueHeap queueHeap2 = new QueueHeap("5");
        QueueHeap queueHeap3 = new QueueHeap("9");
        QueueHeap queueHeap4 = new QueueHeap("7");
        queueHeapMin.enqueue(queueHeap0);
        queueHeapMin.enqueue(queueHeap1);
        queueHeapMin.enqueue(queueHeap2);
        queueHeapMin.enqueue(queueHeap3);
        queueHeapMin.enqueue(queueHeap4);
        System.out.println("往队列内添加元素‘10’、‘9’、‘8’、‘7’、‘5’");
        System.out.println("移除用堆实现的队列的队头：" + queueHeapMin.dequeue());
        System.out.println("用堆实现的队列的元素数量：" + queueHeapMin.size());
        System.out.println("查找用堆实现的队列的队头：" + queueHeapMin.first());
        System.out.println("输出用堆实现的队列的内容(以堆的形式)：" + queueHeapMin.toHeapString());
        System.out.println("输出用堆实现的队列的内容(以队列的形式)：" + queueHeapMin.toQueueString());
        System.out.println("移除用堆实现的队列的队头：" + queueHeapMin.dequeue());
        System.out.println("移除用堆实现的队列的队头：" + queueHeapMin.dequeue());
        System.out.println("移除用堆实现的队列的队头：" + queueHeapMin.dequeue());
        System.out.println("移除用堆实现的队列的队头：" + queueHeapMin.dequeue());
    }
}
