package ShiYan2;

import fourthweek.Array.ArrayUnorderedList;
import seventhweek.BinaryTreeNode;
import seventhweek.LinkedBinaryTree;

import java.util.StringTokenizer;

public class InfixSuffixTree  extends LinkedBinaryTree{
    public BinaryTreeNode change(String strings){
        //把“=”进行抠出
        StringTokenizer tokenizer = new StringTokenizer(strings,"=");
        String string = tokenizer.nextToken();
        //开始转换
        StringTokenizer stringTokenizer = new StringTokenizer(string," ");
        ArrayUnorderedList<String> arrayUnorderedList1 = new ArrayUnorderedList<String>();
        ArrayUnorderedList<LinkedBinaryTree> arrayUnorderedList2 = new ArrayUnorderedList<LinkedBinaryTree>();
        while (stringTokenizer.hasMoreTokens()){
            strings = stringTokenizer.nextToken();
            //判断式子中是否有括号
            boolean judge1 = true;
            if(strings.equals("(")){
                String string1 = "";
                while (judge1){
                    strings = stringTokenizer.nextToken();
                    //开始查询括号的另一个，如果查到的话就会跳出该循环
                    if (!strings.equals(")"))
                        string1 += strings + " ";
                    else
                        break;
                }
                LinkedBinaryTree linkedBinaryTree = new LinkedBinaryTree();
                linkedBinaryTree.root = (change(string1));
                arrayUnorderedList2.addToRear(linkedBinaryTree);
                continue;
            }
            //判断运算符是否是加减的运算
            if((strings.equals("+")|| strings.equals("-"))){
                arrayUnorderedList1.addToRear(strings);
            }else
                ///判断运算符是否是乘除的运算
                if((strings.equals("*")|| strings.equals("/"))){
                LinkedBinaryTree left = arrayUnorderedList2.removeLast();
                String strings2 = strings;
                strings = stringTokenizer.nextToken();
                if(!strings.equals("(")) {
                    LinkedBinaryTree right = new LinkedBinaryTree(strings);
                    LinkedBinaryTree node = new LinkedBinaryTree(strings2, left, right);
                    arrayUnorderedList2.addToRear(node);
                }else {
                    String string3 = "";
                    boolean judge2 = true;
                    while (judge2){
                        strings = stringTokenizer.nextToken();
                        if (!strings.equals(")"))
                            string3 += strings + " ";
                        else break;
                    }
                    LinkedBinaryTree linkedBinaryTree = new LinkedBinaryTree();
                    linkedBinaryTree.root = (change(string3));
                    LinkedBinaryTree node = new LinkedBinaryTree(strings2,left,linkedBinaryTree);
                    arrayUnorderedList2.addToRear(node);
                }
            }else
                arrayUnorderedList2.addToRear(new LinkedBinaryTree(strings));
        }

        while(arrayUnorderedList1.size()>0){
            LinkedBinaryTree left = arrayUnorderedList2.removeFirst();
            LinkedBinaryTree right = arrayUnorderedList2.removeFirst();
            String oper = arrayUnorderedList1.removeFirst();
            LinkedBinaryTree node = new LinkedBinaryTree(oper,left,right);
            arrayUnorderedList2.addToFront(node);
        }
        root = (arrayUnorderedList2.removeFirst()).getRootNode();

        return root;
    }
    public String toString(){
        String result = "";
        ArrayIterator<String> list = new ArrayIterator<String>();
        root.postorder(list);
        for(String i : list){
            result += i+" ";
        }
        return result;
    }
}

