package ShiYan2;

import seventhweek.LinkedBinaryTree;
import seventhweek.PostfixEvaluator;

import java.util.Scanner;

public class fourTest {
    public static void main(String[] args) {
        InfixSuffixTree tree = new InfixSuffixTree();
        LinkedBinaryTree<String> linkedBinaryTree = new LinkedBinaryTree<String>();
        PostfixEvaluator evaluator = new PostfixEvaluator();
        String temp1, temp2, another = "y";
        Scanner scan = new Scanner(System.in);
        while(another.equalsIgnoreCase("y")){
            System.out.print("Please enter what you want to calculate:：");
            temp1 = scan.nextLine();
            linkedBinaryTree.root = (tree.change(temp1));
            temp2 = tree.toString();
            System.out.println("Answer：" + evaluator.evaluate(temp2));
            System.out.println("Do you want to try again?(yes/no):");
            another = scan.nextLine();
            System.out.println();
        }
    }
}
