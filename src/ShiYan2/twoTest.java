package ShiYan2;

import seventhweek.LinkedBinaryTree;

import java.util.Scanner;
import java.util.StringTokenizer;

public class twoTest {
    public static void main(String args[]){
        Scanner scan  = new Scanner(System.in);
        System.out.println("请输入树的前序输出：");
        String temp1 = scan.nextLine();
        System.out.println("请输入树的中序输出：");
        String temp2 = scan.nextLine();
        StringTokenizer TEMP1 = new StringTokenizer(temp1, " ");
        StringTokenizer TEMP2 = new StringTokenizer(temp2, " ");
        int a = 0, b = 0;
        String[] string1 = new String[TEMP1.countTokens()];
        String[] string2 = new String[TEMP2.countTokens()];
        while (TEMP1.hasMoreTokens())
        {
            string1[a] = TEMP1.nextToken();
            a++;
        }
        while (TEMP2.hasMoreTokens())
        {
            string2[b] = TEMP2.nextToken();
            b++;
        }
        PreIntree preIntree = new PreIntree();
        LinkedBinaryTree<String> binaryTree = preIntree.InPreTree(string2,string1);
        System.out.println(binaryTree.toString());
    }
}
