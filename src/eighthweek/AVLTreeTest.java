package eighthweek;

public class AVLTreeTest {
    public static void main(String[] args) {

        AVLTree<String> tree = new AVLTree<String>("谭鑫");
        tree.addElement("北京电子科技学院");
        tree.addElement("网络空间安全系");
        tree.addElement("信息管理与信息技术");
        tree.addElement("YC404");
        tree.addElement("20172305");

        System.out.print("\n前序输出： ");
        tree.toPreString();

        System.out.print("\n中序输出： ");
        tree.toInString();

        System.out.print("\n后序输出： ");
        tree.toPostString();

        System.out.print("\n层序输出： ");
        tree.toLevelString();

        System.out.println("\n树的全输出：" + tree.toString());

        System.out.println("AVL树的高度：" + tree.height());
        System.out.println("AVL树最小值：" + tree.findMin(tree.root));
        System.out.println("AVL树最大值：" + tree.findMax(tree.root));

        System.out.println("\n删除结点‘网络空间安全系’：");
        tree.removeElement("网络空间安全系");
        System.out.println("\n树的全输出：" + tree.toString());
    }
}