package eighthweek;

public class LinkedBinarySearchTreeTest {
    public static void main(String argsp[]){
        LinkedBinarySearchTree<Integer> nodes = new LinkedBinarySearchTree(2);
        nodes.addElement(2);
        nodes.addElement(8);
        nodes.addElement(20);
        nodes.addElement(1);
        nodes.addElement(876);
        nodes.addElement(2);
        nodes.addElement(1000);
        nodes.addElement(3);
        nodes.addElement(1);
        nodes.addElement(10);
        nodes.addElement(19);
        nodes.addElement(19);
        System.out.println("二叉查找树：\n" + nodes.toString());
        System.out.println("删除该树的所有元素‘1’，所得的二叉查找树：");
        nodes.removeAllOccurrences(1);
        System.out.println(nodes.toString());
        System.out.print("删除该树的最大元素\'" + nodes.findMax());
        nodes.removeMax();
        System.out.println("\'，所得的二叉查找树：" + nodes.toString());
        System.out.print("删除该树的最小元素\'" + nodes.findMin());
        nodes.removeMin();
        System.out.println("\'，所得的二叉查找树：" + nodes.toString());
        System.out.println("删除该树的元素‘19’，所得的二叉查找树：");
        nodes.removeElement(19);
        System.out.println(nodes.toString());
    }
}
