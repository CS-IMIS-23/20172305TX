public class PP1_5
{
  //--------------------------------------------------------------
  //Prints a song <<Million Reasons>> from Lada gaga.
  //Harmony is marked in quotation marks.
  //--------------------------------------------------------------
  public static void main(String[] args)
  {
     System.out.println("If I had a highway I would run for the hills");  
     
     System.out.println("If you could find a dry way I'd forever be still");

     System.out.println("But you're giving me a million reasons");
    
     System.out.println("\"Give me a million reasons\"");
  
     System.out.println("\"Givin' me a million reasons.About a million reasons\"");
     System.out.println("I bow down to pray.I try to make the worst seem better");
   }
}
