package com.example.administrator.shiyan3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.StringTokenizer;

public class Main3Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        final EditText editText1 =(EditText) findViewById(R.id.editText1);
        final EditText editText2 = (EditText)findViewById(R.id.editText2);
        //没问题选择排序
        Button button1 = (Button)findViewById(R.id.button1);
        button1.setOnClickListener (new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String temp1 = editText1.getText().toString();
                StringTokenizer NUM = new StringTokenizer(temp1, " ");
                final Integer[] comparables = new Integer[NUM.countTokens()];
                int a = 0;
                while (NUM.hasMoreTokens()) {
                    comparables[a] = Integer.valueOf(NUM.nextToken());
                    a++;
                }
                Sorting.selectionSort(comparables);
                String temp2 = "";
                for(Comparable comparable : comparables){
                    temp2 += comparable + " ";
                }
                editText2.setText(temp2);
            }
        });
        //没问题插入排序
        Button button2 = (Button)findViewById(R.id.button2);
        button2.setOnClickListener (new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String temp1 = editText1.getText().toString();
                StringTokenizer NUM = new StringTokenizer(temp1, " ");
                final Integer[] comparables = new Integer[NUM.countTokens()];
                int a = 0;
                while (NUM.hasMoreTokens()) {
                    comparables[a] = Integer.valueOf(NUM.nextToken());
                    a++;
                }
                Sorting.insertionSort(comparables);
                String temp2 = "";
                for(Comparable comparable : comparables){
                    temp2 += comparable + " ";
                }
                editText2.setText(temp2);
            }
        });
        //没问题冒泡排序
        Button button3 = (Button)findViewById(R.id.button3);
        button3.setOnClickListener (new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String temp1 = editText1.getText().toString();
                StringTokenizer NUM = new StringTokenizer(temp1, " ");
                final Integer[] comparables = new Integer[NUM.countTokens()];
                int a = 0;
                while (NUM.hasMoreTokens()) {
                    comparables[a] = Integer.valueOf(NUM.nextToken());
                    a++;
                }
                Sorting.bubbleSort(comparables);
                String temp2 = "";
                for(Comparable comparable : comparables){
                    temp2 += comparable + " ";
                }
                editText2.setText(temp2);
            }
        });
        //没问题快速排序
        Button button4 = (Button)findViewById(R.id.button4);
        button4.setOnClickListener (new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String temp1 = editText1.getText().toString();
                StringTokenizer NUM = new StringTokenizer(temp1, " ");
                final Integer[] comparables = new Integer[NUM.countTokens()];
                int a = 0;
                while (NUM.hasMoreTokens()) {
                    comparables[a] = Integer.valueOf(NUM.nextToken());
                    a++;
                }
                Sorting.quickSort(comparables);
                String temp2 = "";
                for(Comparable comparable : comparables){
                    temp2 += comparable + " ";
                }
                editText2.setText(temp2);
            }
        });
        //没问题二叉树排序
        Button button5 = (Button)findViewById(R.id.button5);
        button5.setOnClickListener (new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String temp1 = editText1.getText().toString();
                    StringTokenizer NUM = new StringTokenizer(temp1, " ");
                    final Integer[] comparables = new Integer[NUM.countTokens()];
                    int a = 0;
                    while (NUM.hasMoreTokens()) {
                        comparables[a] = Integer.valueOf(NUM.nextToken());
                        a++;
                    }
                Sorting.binarytreeSort(comparables);
                String temp2 = "";
                for(Comparable comparable : comparables){
                    temp2 += comparable + " ";
                }
                editText2.setText(temp2);
                } catch (NonComparableElementException e) {
                    e.printStackTrace();
                } catch (EmptyCollectionException e) {
                    e.printStackTrace();
                }
            }
        });
        //没问题快归并排序
        Button button6 = (Button)findViewById(R.id.button6);
        button6.setOnClickListener (new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String temp1 = editText1.getText().toString();
                StringTokenizer NUM = new StringTokenizer(temp1, " ");
                final Integer[] comparables = new Integer[NUM.countTokens()];
                int a = 0;
                while (NUM.hasMoreTokens()) {
                    comparables[a] = Integer.valueOf(NUM.nextToken());
                    a++;
                }
                Sorting.mergeSort(comparables,0,comparables.length - 1);
                String temp2 = "";
                for(Comparable comparable : comparables){
                    temp2 += comparable + " ";
                }
                editText2.setText(temp2);
            }
        });

        Button button7 = (Button)findViewById(R.id.button7);
        button7.setOnClickListener (new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String temp1 = editText1.getText().toString();
                    StringTokenizer NUM = new StringTokenizer(temp1, " ");
                    final Integer[] comparables = new Integer[NUM.countTokens()];
                    int a = 0;
                    while (NUM.hasMoreTokens()) {
                        comparables[a] = Integer.valueOf(NUM.nextToken());
                        a++;
                    }
                    Sorting.heapSort(comparables);
                    for(Comparable comparable : comparables){
                        System.out.println(comparable);
                    }
                    String temp2 = "";
                    for(Comparable comparable : comparables){
                        temp2 += comparable + " ";
                    }

                    editText2.setText(temp2);
                } catch (EmptyCollectionException e) {
                    e.printStackTrace();
                }
            }
        });

        //没问题哈希排序
        Button button8 = (Button)findViewById(R.id.button8);
        button8.setOnClickListener (new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String temp1 = editText1.getText().toString();
                StringTokenizer NUM = new StringTokenizer(temp1, " ");
                final Integer[] comparables = new Integer[NUM.countTokens()];
                int a = 0;
                while (NUM.hasMoreTokens()) {
                    comparables[a] = Integer.valueOf(NUM.nextToken());
                    a++;
                }
                Sorting.shellSort(comparables);
                String temp2 = "";
                for(Comparable comparable : comparables){
                    temp2 += comparable + " ";
                }
                editText2.setText(temp2);
            }
        });

    }
}
