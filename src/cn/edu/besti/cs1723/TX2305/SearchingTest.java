package cn.edu.besti.cs1723.TX2305;

public class SearchingTest {
    public static void main(String[] args) {
        String[] strings1 = {"谭鑫","20172305","北京电子科技学院","信息管理与信息系统"};
        int[] ints2 = {1,5,15,22,25,31,39,42,47,49,59,68,88};
        int[] ints3  = {13, 19, 27, 26, 28, 30, 38 };
        Comparable[] comparables4= { 1, 5, 15, 22, 25, 31, 39, 42, 47, 49, 59, 68, 88 };

        //顺序查找
        System.out.println("顺序查找是否含有‘20172305’：" + Searching.LinearSearch(strings1,0,strings1.length,"20172305"));
        //二分查找
        System.out.println("二分查找是否含有‘15’：" + Searching.InsertionSearch(ints2,15));
        //折半查找
        System.out.println("折半查找是否含有‘47’：在" + Searching.InsertionSearch(ints2,47) + "位置");
        //斐波那契查找
        System.out.println("斐波那契查找是否含有‘39’：在" + Searching.FibonacciSearch(comparables4,39) + "位置");
        //二叉树查找
        System.out.println("二叉树查找是否含有‘88’：" + Searching. BinaryTreeSearch(comparables4,88));
        //分块查找
        System.out.println("分块查找是否含有‘5’：" + Searching. BlockSearch(comparables4,5,2));
        //哈希查找
        int hashLength = 13;
        int[] hash = new int[hashLength];
        for (int i = 0; i < ints3.length; i++)
        {
            Searching.insertHash(hash, hashLength, ints3[i]);
        }
        System.out.println("哈希查找是否含有‘13’" + Searching.searchHash(hash,hashLength, 27));
    }
}
