package cn.edu.besti.cs1723.TX2305;

public class SortingTest {
    public static void main(String[] args) {
        Integer[] integers1 = {23, 76, 3, 9, 187, 55, 90, 2, 4};
        String[] strings2 = {"谭鑫", "网络空间安全系", "信息管理与信息系统", "20172305"};
        Integer[] integers3 = {23, 5, 87, 45, 90, 78, 33, 34, 77};
        Integer[] integers4 = {22, 44, 78, 98, 66, 87, 90, 254, 6};
        Integer[] integers5 = {3, 1, 322, 89, 17, 5, 90, 2, 4};
        Integer[] integers6 = {230, 5, 31, 900, 1, 521, 9, 21, 40};
        Integer[] integers7 = {3, 7, 300, 989, 17, 5, 911, 20, 41};
        Integer[] integers8 = {2, 90, 3, 966, 107, 52, 9, 203, 4};
        System.out.println("插入排序：");
        for(Integer integer: integers1)
            System.out.print(integer + " ");
        System.out.println();
        Sorting.insertionSort(integers1);
        for(Integer integer: integers1)
            System.out.print(integer + " ");
        System.out.println();

        System.out.println("冒泡排序：");
        for(String string : strings2)
            System.out.print(string + " ");
        System.out.println();
        Sorting.bubbleSort(strings2);
        for(String string: strings2)
            System.out.print(string + " ");
        System.out.println();

        System.out.println("二叉树排序：");
        for(Integer integer: integers3)
            System.out.print(integer + " ");
        System.out.println();
        Sorting.binarytreeSort(integers3);
        System.out.println();

        System.out.println("堆排序：");
        for(Integer integer: integers4)
            System.out.print(integer + " ");
        System.out.println();
        Sorting.heapSort(integers4);
        for(Integer integer: integers4)
            System.out.print(integer + " ");
        System.out.println();

        System.out.println("归并排序：");
        for(Integer integer: integers5)
            System.out.print(integer + " ");
        System.out.println();
        Sorting.mergeSort(integers5,0,integers5.length - 1);
        for(Integer integer: integers5)
            System.out.print(integer + " ");
        System.out.println();

        System.out.println("选择排序：");
        for(Integer integer: integers6)
            System.out.print(integer + " ");
        System.out.println();
        Sorting.selectionSort(integers6);
        for(Integer integer: integers6)
            System.out.print(integer + " ");
        System.out.println();

        System.out.println("快速排序：");
        for(Integer integer: integers7)
            System.out.print(integer + " ");
        System.out.println();
        Sorting.quickSort(integers7);
        for(Integer integer: integers7)
            System.out.print(integer + " ");
        System.out.println();

        System.out.println("希尔排序：");
        for(Integer integer: integers8)
            System.out.print(integer + " ");
        System.out.println();
        Sorting.shellSort(integers8);
    }
}
