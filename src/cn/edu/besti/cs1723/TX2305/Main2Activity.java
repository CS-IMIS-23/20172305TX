package com.example.administrator.shiyan3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.StringTokenizer;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.T;

//查找
public class Main2Activity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        final EditText editText1 =(EditText) findViewById(R.id.editText1);
        final EditText editText2 = (EditText)findViewById(R.id.editText2);

        Button button1 = (Button)findViewById(R.id.button1);
        button1.setOnClickListener (new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String temp = editText1.getText().toString();
                StringTokenizer NUM = new StringTokenizer(temp, " ");
                String[] comparables = new String[NUM.countTokens()];
                int a = 0;
                while (NUM.hasMoreTokens()) {
                    comparables[a] = NUM.nextToken();
                    a++;
                }
                System.out.println(Searching.LinearSearch(comparables,0,comparables.length - 1,editText2.getText().toString()));
                if(Searching.LinearSearch(comparables,0,comparables.length,editText2.getText().toString())){
                    Toast toast = Toast.makeText(Main2Activity.this, "true!", Toast.LENGTH_LONG);
                    toast.show();
                }else {
                    Toast toast = Toast.makeText(Main2Activity.this, "false!", Toast.LENGTH_LONG);
                    toast.show();
                }
            }
        });

        Button button2 = (Button)findViewById(R.id.button2);
        button2.setOnClickListener (new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String temp = editText1.getText().toString();
                StringTokenizer NUM = new StringTokenizer(temp, " ");
                Comparable[] comparables = new Comparable[NUM.countTokens()];
                Integer[] integers = new Integer[NUM.countTokens()];
                int a = 0;
                while (NUM.hasMoreTokens()) {
                    comparables[a] = (Comparable) NUM.nextToken();
                    integers[a] = Integer.parseInt(String.valueOf(comparables[a]));
                    a++;
                }
                if(Searching.BinarySearch(comparables, 0, comparables.length, editText2.getText().toString())){
                    Toast toast = Toast.makeText(Main2Activity.this, "true!", Toast.LENGTH_LONG);
                    toast.show();
                }else {
                    Toast toast = Toast.makeText(Main2Activity.this, "false!", Toast.LENGTH_LONG);
                    toast.show();
                }
            }
        });

        Button button3 = (Button)findViewById(R.id.button3);
        button3.setOnClickListener (new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String temp = editText1.getText().toString();
                    StringTokenizer NUM = new StringTokenizer(temp, " ");
                    final Comparable[] comparables = new Comparable[NUM.countTokens()];
                    final Integer[] integers = new Integer[NUM.countTokens()];
                    int a = 0;
                    while (NUM.hasMoreTokens()) {
                        comparables[a] = (Comparable) NUM.nextToken();
                        integers[a] = Integer.parseInt(String.valueOf(comparables[a]));
                        a++;
                    }
                    if(!Searching.BinaryTreeSearch(comparables,editText2.getText().toString())){
                        Toast toast = Toast.makeText(Main2Activity.this, "true!", Toast.LENGTH_LONG);
                        toast.show();
                    }else {
                        Toast toast = Toast.makeText(Main2Activity.this, "false!", Toast.LENGTH_LONG);
                        toast.show();
                    }
                } catch (NonComparableElementException e) {
                    e.printStackTrace();
                } catch (EmptyCollectionException e) {
                    e.printStackTrace();
                } catch (ElementNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

        Button button4 = (Button)findViewById(R.id.button4);
        button4.setOnClickListener (new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String temp = editText1.getText().toString();
                StringTokenizer NUM = new StringTokenizer(temp, " ");
                Comparable[] comparables = new Comparable[NUM.countTokens()];
                Integer[] integers = new Integer[NUM.countTokens()];
                int a = 0;
                while (NUM.hasMoreTokens()) {
                    comparables[a] = (Comparable) NUM.nextToken();
                    integers[a] = Integer.parseInt(String.valueOf(comparables[a]));
                    a++;
                }
                if(Searching. BlockSearch(comparables,editText2.getText().toString(),comparables.length)){
                    Toast toast = Toast.makeText(Main2Activity.this, "true!", Toast.LENGTH_LONG);
                    toast.show();
                }else {
                    Toast toast = Toast.makeText(Main2Activity.this, "false!", Toast.LENGTH_LONG);
                    toast.show();
                }
            }
        });

        Button button5 = (Button)findViewById(R.id.button5);
        button5.setOnClickListener (new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String temp = editText1.getText().toString();
                StringTokenizer NUM = new StringTokenizer(temp, " ");
                Comparable[] comparables = new Comparable[NUM.countTokens()];
                Integer[] integers = new Integer[NUM.countTokens()];
                int a = 0;
                while (NUM.hasMoreTokens()) {
                    comparables[a] = (Comparable) NUM.nextToken();
                    integers[a] = Integer.parseInt(String.valueOf(comparables[a]));
                    a++;
                }
                System.out.println();
                if(Searching.InsertionSearch(integers, Integer.parseInt(editText2.getText().toString())) > 0){
                    Toast toast = Toast.makeText(Main2Activity.this, "true!", Toast.LENGTH_LONG);
                    toast.show();
                }
                else{
                    Toast toast = Toast.makeText(Main2Activity.this, "false!", Toast.LENGTH_LONG);
                    toast.show();
                }
            }
        });

        Button button6 = (Button)findViewById(R.id.button6);
        button6.setOnClickListener (new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String temp = editText1.getText().toString();
                StringTokenizer NUM = new StringTokenizer(temp, " ");
                Comparable[] comparables = new Comparable[NUM.countTokens()];
                Integer[] integers = new Integer[NUM.countTokens()];
                int a = 0;
                while (NUM.hasMoreTokens()) {
                    comparables[a] = (Comparable) NUM.nextToken();
                    integers[a] = Integer.parseInt(String.valueOf(comparables[a]));
                    a++;
                }
                if((Integer)Searching.FibonacciSearch(comparables,Integer.parseInt(editText2.getText().toString())) > 0){
                    Toast toast = Toast.makeText(Main2Activity.this, "true!", Toast.LENGTH_LONG);
                    toast.show();
                }
                else{
                    Toast toast = Toast.makeText(Main2Activity.this, "false!", Toast.LENGTH_LONG);
                    toast.show();
                }
            }
        });

        Button button7 = (Button)findViewById(R.id.button7);
        button7.setOnClickListener (new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String temp = editText1.getText().toString();
                StringTokenizer NUM = new StringTokenizer(temp, " ");
                Comparable[] comparables = new Comparable[NUM.countTokens()];
                Integer[] integers = new Integer[NUM.countTokens()];
                int a = 0;
                while (NUM.hasMoreTokens()) {
                    comparables[a] = (Comparable) NUM.nextToken();
                    integers[a] = Integer.parseInt(String.valueOf(comparables[a]));
                    a++;
                }
                int hashLength = 13;
                int[] hash = new int[hashLength];
                for (int i = 0; i < integers.length; i++)
                {
                    Searching.insertHash(hash, hashLength, integers[i]);
                }
                if(Searching.HashSearch(hash,hashLength, 27) < 0){
                    Toast toast = Toast.makeText(Main2Activity.this, "true!", Toast.LENGTH_LONG);
                    toast.show();
                }
                else{
                    Toast toast = Toast.makeText(Main2Activity.this, "false!", Toast.LENGTH_LONG);
                    toast.show();
                }
            }
        });
    }
}
