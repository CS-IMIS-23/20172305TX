package seventhweek;

public class HashTest {
    public static void main(String args[]){
        //学号05，加21后为26
        int[] num = {11,78,10,1,3,2,4,21,26};
        System.out.println("列表中是否含有元素‘20’：" + HushSearch.hashSearch(num,20));
        System.out.println("列表中是否含有元素‘78’：" + HushSearch.hashSearch(num,78));
        System.out.println("列表中是否含有元素‘1’：" + HushSearch.hashSearch(num,1));
        System.out.println("冲突次数：" + HushSearch.times() + "次");
        System.out.println("平均查找长度(ASL)：" + HushSearch.ASL());
    }
}
