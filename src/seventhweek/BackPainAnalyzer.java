package seventhweek;

        import java.io.FileNotFoundException;

public class BackPainAnalyzer {

    /**
     * Asks questions of the user to diagnose a medical problem.
     */
    public static void main(String[] args) throws FileNotFoundException {
        System.out.println("So, you're having back  .");
        DecisionTree expert = new DecisionTree("input.txt");
        expert.evaluate();
        System.out.println("决策树的深度：" + expert.getHeight());
        System.out.println("决策树的叶结点数目：" + expert.getNumber());
        System.out.println("层序输出(递归版)：");
        expert.String1();
        System.out.println("\n层序输出(非递归版)：");
        expert.String2();
    }
}
