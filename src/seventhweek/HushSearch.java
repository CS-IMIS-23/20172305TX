package seventhweek;

import java.text.DecimalFormat;

public class HushSearch {
    //创建散列数组
    private static intNode[] array;
    //创建冲突次数
    private static int count;
    //创建总排序次数
    private static int times;
    public static boolean hashSearch(int[] arr, int node){
        store(arr);
        boolean found;
//        int temp = node % arr.length;
        int temp = node % 11;
        if(array[temp] != null){
            if(array[temp].number == node)
                found = true;
            else{
                intNode head = array[temp];
                while(head.next != null && head.number != node)
                    head = head.next;
                if(head.number == node)
                    found = true;
                else
                    found = false;
            }
        }else{
            found = false;
        }
        return found;
    }
    private static void store(int[] data){
        array = new intNode[11];
        int n = 0;
        while(n < data.length){
//            int num = (data[n]) % data.length;
            int num = (data[n]) % 11;
            intNode temp = new intNode(data[n]);
            if(array[num] == null) {
                array[num] = temp;
            }else{
                intNode intNode = array[num];
                while(intNode.next != null){
                    intNode = intNode.next;
                    count++;
                    times++;
                }
                intNode.next = temp;
                count++;
                times++;
            }
            n++;
            times++;
        }
    }
    //我的冲突次数总是实际冲突次数的三倍，所以就在结果上进行除3
    public static int times(){
        return count / 3;
    }
    public static String ASL(){
        DecimalFormat format = new DecimalFormat("0.###");
        return format.format((double)times / (times - count));
    }
    private static class intNode{
        public int number;
        public intNode next;
        public intNode(int figure){
            number = figure;
            next = null;
        }
    }
}