package seventhweek;

public class LinkedBinaryTreeTest {
    public static void main(String args[]){
        LinkedBinaryTree<String> stringLinkedBinaryTree1 = new LinkedBinaryTree<>("20172305");
        LinkedBinaryTree<String> stringLinkedBinaryTree2 = new LinkedBinaryTree<>("YC404");
        LinkedBinaryTree<String> stringLinkedBinaryTree3 = new LinkedBinaryTree<>("网络空间安全系");
        LinkedBinaryTree<String> stringLinkedBinaryTree4 = new LinkedBinaryTree<>("吉林省");
        LinkedBinaryTree node = new LinkedBinaryTree();
        System.out.println("此树是否为空：" + node.isEmpty());
        LinkedBinaryTree node1 = new LinkedBinaryTree("信息管理与信息系统",stringLinkedBinaryTree1, stringLinkedBinaryTree2);
        LinkedBinaryTree node2 = new LinkedBinaryTree("北京电子科技学院",stringLinkedBinaryTree3,node1);
        LinkedBinaryTree node3 = new LinkedBinaryTree("谭鑫",node2,stringLinkedBinaryTree4);
        System.out.println(node3.toString());
        System.out.print("前序输出：");
        node3.toPreString();
        System.out.print("\n中序输出：");
        node3.toInString();
        System.out.print("\n后序输出：");
        node3.toPostString();
        System.out.print("\n层序输出：");
        node3.toLevelString();
        System.out.println("\n结点‘YC404’是否是内部节点：" + stringLinkedBinaryTree2.getRootNode().internalNode());
        System.out.println("结点‘吉林省’是否是内部节点：" + stringLinkedBinaryTree4.getRootNode().internalNode());
        System.out.println(node3.getHeight());
        System.out.println("此树是否含有‘20172305’" + node3.contains("20172305"));
        System.out.print("删除此树‘谭鑫’结点右侧的子结点(结果进行前序输出)：");
        node3.removeRightSubtree("谭鑫");
        node3.toPreString();
        System.out.print("\n删除此树的所有结点：");
        node3.removeElement();
        node3.toPreString();
        System.out.println();
        System.out.println(node3.toString());
    }
}
