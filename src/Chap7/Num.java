//**************************************************************************
//  Num.java      Author: Lewis/Loftus
//
//  Represents a single integer as an object.
//**************************************************************************

public class Num
{
    private int value;
   
    //----------------------------------------------------------------------
    //    Sets up the new Num objects. storing an initial value.
    //----------------------------------------------------------------------
    public Num(int update)
    {
         value = update;
    }
    
    //----------------------------------------------------------------------
    //    Sets the stored value top the newly specified value.
    //----------------------------------------------------------------------
    public void setValue(int update)
    {
         value = update;
    }
 
    //----------------------------------------------------------------------
    //    Returns the stored integer value as a sting
    //----------------------------------------------------------------------
    public String toString()
    {
         return value + "";
    }
}
