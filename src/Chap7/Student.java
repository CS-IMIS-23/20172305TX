//******************************************************************************
//  Student.java           Author: Lewis/Loftus
//
//  Represents a college syudent.
//******************************************************************************
public class Student
{
   private String firstName, lastName;
   private Address homeAddress, schoolAddress;
   //-------------------------------------------------------------------------
   //  Contructor: Sets up this studend with the specified values.
   //-------------------------------------------------------------------------

   public Student(String first, String last, Address home, Address school)
   {
        firstName = first;
        lastName = last;
        homeAddress = home;
        schoolAddress = school;
   }

   //-------------------------------------------------------------------------
   //  Returns a string decription of this Student object.
   //-------------------------------------------------------------------------
   public String toString()
   {
        String result;

        result = firstName + " " + lastName + "\n";
        result += "Home Address:\n" + homeAddress + "\n";
        result += "School Address:\n" + schoolAddress;

        return result;
   }
}
