//*************************************************************************************
//   PP5_7.java     	Author: sanjin
//
//   Man-Machine confrontation simulated stone shears.
//*************************************************************************************
import java.util.*;
public class PP5_7
{
  public static void main(String[] args)
  {
    String s1, s2, another = "y";
    int a = 0, b = 0, c = 0, num;
    
    while (another.equalsIgnoreCase("y"))
    {
       // man 
       Scanner scan = new Scanner(System.in);
       System.out.print("Enter 'stone--cloth--scissors': ");
       s1 = scan.nextLine();

       // computer
       ArrayList<String> play = new ArrayList<>();
       play.add("stone");
       play.add("scissors");
       play.add("cloth");

       num = (int)Math.random() * 3;
       s2 = play.get(num);
       System.out.println("yours: " + s1); 
       System.out.println("computer: " + s2);

       //  compare
       if (s1.equals(s2))
         { System.out.println("Dogfall");
           c++;}
       else
         { if (s1.equals("stone") || s1.equals("scissors") || s1.equals("cloth")) 
             { if ((s1.equals("stone") && s2.equals("scissors")) || (s1.equals("scissors") && s2.equals("cloth")) || (s1.equals("cloth") && s2.equals("stone")))
             { System.out.println("Mankind Win");
               a++;}
               else
                 { System.out.println("Machine Win");
                    b++;}}
            else
              System.out.println("注意：输入为石头剪刀布");}
       System.out.print("continue (y/n)? ");
       another = scan.nextLine();
       System.out.println();}
      
      System.out.println();
      System.out.println("Machine Win: " + b);
      System.out.println("Mankind Win: " + a);
      System.out.println("Dogfall: " + c);
 }
}
