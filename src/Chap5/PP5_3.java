//*************************************************************************************
//  PP5_3.java 			Author: sanjin
//
//  Determine the odd number , even number and zero contained in an integer.
//*************************************************************************************

import java.util.Scanner;

public class PP5_3
{
  public static void main(String[] args)
  {
   int a, num;
   int c = 0;
   int d = 0;
   int b = 0;
  
   Scanner scan = new Scanner(System.in);
   System.out.print("Enter a number(-2147483648~~2147483647): ");
   num = scan.nextInt();
  
   if (num == 0)
      d++;
   else{
      while(num != 0)
      { a = (num % 10);
  
        if (a % 2 != 0) // judging odd number
           c++;
        else{
           if (a == 0) // judging zero
              d++;
           else 
              b++;} // judging even number 
              num = num / 10;}}
  

   System.out.println("Odd number: " + c + "\nEven number: " + b +"\nZero: " + d); 
  }
}
