//********************************************************************************************
//  PP5_1.java            Author: sanjin
//
//  Read the integer entered by the user as the year.Determine whether a year is a leap year.
//********************************************************************************************
import java.util.Scanner;

public class PP5_1
{
 public static void main(String[] args)
 {
   final int MIN = 1582;
   int years;
  
   Scanner scan = new Scanner(System.in);
   System.out.print("Enter year: ");
   years = scan.nextInt();
  
   if (years > MIN)
     if (years%100 != 0) 
     {  if (years%4 == 0 )
         System.out.println("The years you entered is a leap year.");  // Leap year
       else
         System.out.println("The years you entered is a flat year.");}  // Flat year
     else
     { if (years%400 == 0) 
         System.out.println("The years you entered is a leap year.");  // Leap year
       else
         System.out.println("The years you entered is a flat year.");}  // Flat year
   else
     System.out.println("There was no Gregorian Caradendar before 1582"); // No Gregorian calendar
  
  /* if(years > MIN)
    { if((years%4==0 && years%100!=0) || (years%100 == years%400))  
        System.out.println("The years you entered is a leap year.");  // Leap year
      else
        System.out.println("The years you entered is a flat year.");  // Flat year
    } 
   else
     System.out.println("There was no Gregorian Caradendar before 1582"); // No Gregorian calendar*/
 }
}
