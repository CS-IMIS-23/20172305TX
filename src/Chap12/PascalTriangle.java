package Chap12;

import java.io.*;
import java.util.Scanner;

public class PascalTriangle
{
    public static void main(String args[]) throws IOException {
        int str[][];
        int a = 0;
        String temp;
        Scanner scan = new Scanner(System.in);

        FileWriter file = new FileWriter("PascalTriangle.txt");

        System.out.print("请输入杨辉三角的行数：");
        int num = scan.nextInt();
        String[] string = new String[num + 1];

        str = new int[num][num];
        for(int i=0;i<num;i++)
           for(int j=0;j<=i;j++)
               str[i][j] = Pascal(i,j);

        for(int i=0;i < num;i++)
        {
            file.write("\n");
            for(int n=num-i;n>= 1;n--)
                file.write("\t");
            for(int j=0;j<=i;j++)
                file.write("\t" + str[i][j] + "\t");
        }
        file.flush();

        FileReader fr = new FileReader("PascalTriangle.txt");
        BufferedReader br = new BufferedReader(fr);

        while((temp = (br.readLine()))!= null)
        {
            string[a] = temp;
            a++;
        }
        br.close();
        fr.close();

        System.out.println("杨辉三角的第" + num + "行："+ string[num]);
    }

    public static int Pascal(int i,int j)
    {
        if(j==0||j==i)
            return 1;
        else
            return Pascal(i-1,j) + Pascal(i-1,j-1);

    }

}

