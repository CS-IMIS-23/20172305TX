package Chap12;

import java.util.Scanner;

public class PalindromeTesters
{
    public static void main(String[] args) {
        String str, another = "y";
        Scanner scan = new Scanner(System.in);

        while (another.equalsIgnoreCase("Y"))
        {
            System.out.print("Enter a palindrome:");
            str = scan.nextLine();

            if (Palindrome(str))
                  System.out.println("That string IS a palindrome.");
            else
                  System.out.println("That string is NOT a palindrome.");
            System.out.println("Test another(y/n)?");
            another = scan.nextLine();
        }
    }
    public static boolean Palindrome(String str)
    {
        if(str.length()==1)
            return true ;
        else if(str.length()==2)
        {
            if(str.charAt(0)==str.charAt(str.length()-1))
                return true ;
            else
                return false ;
        }
        else if(str.charAt(0)==str.charAt(str.length()-1))
            return Palindrome(str.substring(1,str.length()-1)) ;
        else
            return  false;
    }
}
