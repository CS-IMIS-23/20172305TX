package Chap9;

public abstract class animal
{
        public String name;
        public int id;

        public animal(String name, int id)
        {
            this.name = name;
            this.id = id;
        }
        public abstract void eat();
        public abstract void sleep();
        public abstract void introduction() ;
}

