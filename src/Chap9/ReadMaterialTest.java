package Chap9;

public class ReadMaterialTest
{
    public static void main(String[] args)
    {
        Novel num1 = new Novel(1500,52500, "红高粱","中国","莫言","love");
        Books num2 = new Books(1500,52500,"蓝猫淘气三千问","a","children","picture");
        Magazine num3 = new Magazine(1500,52500,25,"时尚芭莎","苏芒","fashion时尚与娱乐");
        AcademicJournals num4 = new AcademicJournals(1500,52500,"基于独立规则集位提取的包分类压缩方法","country","王孝龙","internet");
        Textbook num5 = new Textbook(1500,52500,"十万个为什么","卢嘉锡","nature",7);
        System.out.println(num1 + "\n\n" + num2 + "\n\n" + num3 + "\n\n" + num4 + "\n\n" + num5);
    }
}
