package Chap9;
import Chap5.Coin;
//*************************************************************************************
//  MonetaryCoin.java           Author: sanjin
//
//  Add a method for the Chap5 coin to return the face value of a coin.
//*************************************************************************************
import java.text.NumberFormat;
public class MonetaryCoin extends Coin
{
   double face2;

    // save the face value of a coin
    public double flip()
    {
        int face1;
        face1 = (int)(Math.random() * 3);
        if (face1 == 0)
            face2 = 0.1;
        else if (face1 == 1)
            face2 = 0.5;
        else
            face2 = 1.0;
        return face2;
    }
    //revise coin value
    public void setFace(double a)
    {
        face2 = a;
    }
    //access coin value
    public double getFace()
    {
        return face2;
    }

    public String toString()
    {
        NumberFormat fmt = NumberFormat.getCurrencyInstance();
        return  "Coin face's :" + fmt.format(face2);
    }
}
