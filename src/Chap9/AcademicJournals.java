package Chap9;

public class AcademicJournals extends ReadMaterial
{
    private  int definitions;
    private String nationality;
    public AcademicJournals(int numPages, int numDefinitions, String name, String nationality, String author, String type)
    {
        super(numPages,name,author,type);
        definitions = numDefinitions;
        this.nationality = nationality;
    }
    public double computeRatio()
    {
        return (double) definitions / pages;
    }
    public void setDefinitions(int numDefinitions)
    {
        definitions = numDefinitions;
    }
    public int getDefinitions()
    {
        return definitions;
    }
    public String getType()
    {
        return type + "lield";
    }
    public String Scholoarship()
    {
        String a = "country", result;
        if (nationality.equals(a))
            result = "It is only for reference within the country and the level of the article has little influence.";
        else
            result = "This academic article has global influence and is of the great reference value.";
         return result;
    }
    public String toString()
    {
        String result;
        result = "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
        result += "Academic articles: " + "《" + name + "》("+nationality+ ")\nDoctor: " + author + "\nAcademic Field :" + type + " field\nNumPages :" + this.getPages() + "\n";
        result += this.Scholoarship();
        return result;
    }
}
