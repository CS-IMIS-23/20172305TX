package Chap9;

public class Novel extends ReadMaterial
{
    private  int definitions;
    private String nationality;
    public Novel(int numPages, int numDefinitions, String name, String nationality, String author, String type)
    {
        super(numPages,name,author,type);
        definitions = numDefinitions;
        this.nationality = nationality;
    }
    public double computeRatio()
    {
        return (double) definitions / pages;
    }
    public void setDefinitions(int numDefinitions)
    {
        definitions = numDefinitions;
    }
    public int getDefinitions()
    {
        return definitions;
    }
    public String toString()
    {
        String result;
        result = "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
        result += "Novel name: 《" + name +"》" + "\nAuthor: " + author + "(" + nationality + ")" + "\nNovel theme :" + type + "NumPages :" + this.getPages();
        return result;
    }
}


