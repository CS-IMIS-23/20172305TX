package Chap9;
//*************************************************************************************
//  MonetaryCoinTest.java           Author: sanjin
//
//  Test MonetaryCoin and Coin.
//*************************************************************************************
import java.text.NumberFormat;
public class MonetaryCoinTest
{
    public static void main(String[] args)
    {
        NumberFormat fmt = NumberFormat.getCurrencyInstance();
        MonetaryCoin coin1 = new MonetaryCoin();
        MonetaryCoin coin2 = new MonetaryCoin();
        MonetaryCoin coin3 = new MonetaryCoin();

        coin1.flip();
        coin2.flip();
        coin3.flip();

        coin1.setFace(0.4);

        System.out.println(coin1);
        System.out.println(coin2);
        System.out.println(coin3);

        System.out.println(coin1.getFace());
        System.out.println(coin2.getFace());
        System.out.println(coin3.getFace());
        System.out.println("Sum: " + fmt.format(coin1.getFace() + coin2.getFace() + coin3.getFace()));
    }
}
