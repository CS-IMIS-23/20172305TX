package Chap9;
//*************************************************************************************
//  Massages.java           Author: Lewis/Loftus
//
//  Demonstrates the use of an overridden method.
//*************************************************************************************
public class Messages
{
    //---------------------------------------------------------------------------------
    // Creates two objects and invokes the messages method in each.
    //---------------------------------------------------------------------------------
    public static void main(String[] args)
    {
        Thought parked = new Thought();
        Advice dates = new Advice();

        parked.messages();

        dates.messages();  //  overridden
    }
}
