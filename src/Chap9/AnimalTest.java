package Chap9;

public class AnimalTest
{
    public static void main(String[] args)
    {
        Sheep sheep = new Sheep("沸羊羊",1111);
        Cow cow = new Cow("牛魔王",2222);

        System.out.println("--------------------------");
        sheep.eat();
        sheep.sleep();
        sheep.introduction();

        System.out.println("\n--------------------------");
        cow.eat();
        cow.sleep();
        cow.introduction();
    }
}
