package Chap9;

public class Books extends ReadMaterial
{
    private  int definitions;
    private String picture;
    public Books(int numPages, int numDefinitions, String name, String author, String type, String picture)
    {
        super(numPages,name,author,type);
        definitions = numDefinitions;
        this.picture = picture;
    }
    public double computeRatio()
    {
        return (double) definitions / pages;
    }
    public void setDefinitions(int numDefinitions)
    {
        definitions = numDefinitions;
    }
    public int getDefinitions()
    {
        return definitions;
    }
    public String appendix()
    {
        String a = "picture", b = "spell", result;
        if(picture.equals(a))
            result = "This book is illustrated. What's more, it is suitable for teenagers to read.";
        else
            {
                if (picture.equals(b))
                    result = "This book is watched in spelling. What's more, it is suitable for infants to read";
                else
                    result = "The whole text of this book.";
            }
        return result;
    }
    public String toString()
    {
        String result;
        result = "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
        result += "Book name: 《" + name +"》" + "\nAuthor: " + author + "\nNovel theme :" + type + "\nNumPages :" + this.getPages() + "\n";
        result += this.appendix();
        return result;
    }
}
