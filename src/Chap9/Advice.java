package Chap9;
//*****************************************************************************************
//  Advice.java         Author: Lewis/Loftus
//
//  Represent some thoughtful advice. Used to demonstrate the use of an overridden method.
//*****************************************************************************************
public class Advice extends Thought
{
    //---------------------------------------------------------------------------------
    //  Prints a message. This method overrides the parent's version.
    //---------------------------------------------------------------------------------
    public void messages()
    {
        System.out.println("Warning: Dates in calendar are closer " + "than they appear.");

        System.out.println();

        super.messages();  // explicitly invokes the parent's version
    }
}
