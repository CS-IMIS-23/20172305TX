package Chap9;
//***************************************************************************************
//  Dictionary.java         Author: Lewis/Loftus
//
//  Represents a book. Used as the parent of a derived class to demonstrate inheritance.
//***************************************************************************************
public class Dictionary extends Book
{
    private int definitions = 52500;

    //---------------------------------------------------------------------------------
    //  Prints a message using both local and inherited values.
    //---------------------------------------------------------------------------------
    public double computeRatio()
    {
        return (double) definitions/pages;
    }

    //---------------------------------------------------------------------------------
    //  Definition mutator
    //---------------------------------------------------------------------------------
    public void setDefinitions(int numDefinitions)
    {
        definitions = numDefinitions;
    }

    //---------------------------------------------------------------------------------
    //  Definitions accessor.
    // ---------------------------------------------------------------------------------
    public int getDefinitions()
    {
        return definitions;
    }
}