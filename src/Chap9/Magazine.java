package Chap9;

public class Magazine extends ReadMaterial
{
    private  int definitions, essay;
    public Magazine(int numPages, int numDefinitions, int essay, String name, String author, String type)
    {
        super(numPages,name,author,type);
        definitions = numDefinitions;
        this.essay = essay;
    }
    public double computeRatio()
    {
        return (double) definitions / pages;
    }
    public void setDefinitions(int numDefinitions)
    {
        definitions = numDefinitions;
    }
    public int getDefinitions()
    {
        return definitions;
    }
    public int getEssay()
    {
        return essay;
    }
    public String toString()
    {
        String result;
        result = "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
        result += "Magazine name: " + "《" + name +"》" + "\nEdit: " + author + "\nNovel theme :" + type + "\nNumEssay: " + essay + "\nNumPages :" + this.getPages();
        return result;
    }
}
