package Chap9;
//*******************************************************************************************************************
//  Thought.java        Author: Lewis/Loftus
//
//  Represents a stray thought. Used as the parent of derived class to demonstrate the use of an overridden method.
//*******************************************************************************************************************
public class Thought
{
    //---------------------------------------------------------------------------------
    // Prints a messages.
    //---------------------------------------------------------------------------------
    public void messages()
    {
        System.out.println("I feel like I'm diagonally parked in a " + "parallel universe.");

        System.out.println();
    }
}
