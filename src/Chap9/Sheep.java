package Chap9;

public class Sheep extends animal
{
    public Sheep(String name, int id)
    {
        super(name, id);
    }
    public void eat()
    {
        System.out.println("I'm eating!");
    }
    public void sleep()
    {
        System.out.println("I'm sleeping!");
    }
    public void introduction()
    {
        System.out.println("My ID :" + id + "\nMy name: " + name);
    }
}
