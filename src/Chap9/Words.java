package Chap9;

import java.util.Dictionary;

//*************************************************************************************
//  Words.java          Author: Lewis/Loftus
//
//  Demonstrates the use of an inherited method.
//*************************************************************************************
public class Words
{
    //---------------------------------------------------------------------------------
    //  Instantiates a derived class and invokes its inherited and local methods.
    //---------------------------------------------------------------------------------
    public static void main(String[] args)
    {
        Chap9.Dictionary webster = new Chap9.Dictionary();

        System.out.println("Number of pages: " + webster.getPages());

        System.out.println("Number of definitions: " + webster.getDefinitions());

        System.out.println("Definitions per page: " + webster.computeRatio());
    }
}
