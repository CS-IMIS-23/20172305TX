package Chap9;

public class ReadMaterial
{
    protected int pages;
    protected String author, type, name;
    public ReadMaterial(int numpages, String name, String author, String type)
    {
        pages = numpages;
        this.author = author;
        this.type = type;
        this.name = name;
    }
    // 页数
    public void setPages(int numpages)
    {
        pages = numpages;
    }

    public int getPages()
    {
        return pages;
    }
    //作者
    public String getAuthor()
    {
        return author;
    }
    //丛书类型
    public String getType()
    {
        return type;
    }
}
