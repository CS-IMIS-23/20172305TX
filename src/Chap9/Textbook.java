package Chap9;

public class Textbook extends ReadMaterial
{
    private  int definitions, unit;
    public Textbook(int numPages, int numDefinitions, String name, String author, String type, int unit)
    {
        super(numPages,name,author,type);
        definitions = numDefinitions;
        this.unit = unit;
    }
    public double computeRatio()
    {
        return (double) definitions / pages;
    }
    public void setDefinitions(int numDefinitions)
    {
        definitions = numDefinitions;
    }
    public int getDefinitions()
    {
        return definitions;
    }
    public int getUnit()
    {
        return unit;
    }
    public String getType()
    {
        return type + "class";
    }
    public String toString()
    {
        String result;
        result = "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
        result += "TextBook name: "+ "《" + name +"》"  + "\nAuthor: " + author + "\nNovel theme :" + type + " calss\nNumPages :" + this.getPages() + "\n";
        result += "Applicable subject: " + this.getType();
        return result;
    }
}
