//************************************************************************************
// Box.java                 Author: sanjin
//
// 表示箱子的长、宽、高，布尔型变量定义盒子是否装满等方法。
//*************************************************************************************
public class Box
{
 public double a, b, c, d;
 
 public Box(double e, double f, double g, double h)//a为箱子的长度，b为箱子的宽度，c为箱子的高度，d为箱子的装物高度
{ 
 a = e;
 b = f;
 c = g;
 d = h;
}
 //访问箱子的长度
 public double getLong()
{
 return a;
}
 //访问箱子的宽度
 public double getWidth()
{
 return b;
}
 //访问箱子的高度
 public double getHigh()
{
 return c;
}
 //设置箱子的长度
 public void setLong(double long1)
{
 a = long1;
}
 //设置箱子的宽度
 public void setWidth(double width1)
{
 b = width1;
}
 //设置箱子的高度
 public void setHigh(double high1)
{
 c = high1;
}
 //判断箱子是否装满
 public boolean trunkful()
{
 boolean full;
 if (c - d > 0) 
  full = false;  
 else 
  full = true;
 return full;
}
 public String toString()
{
 String sentence = "Basic data of the box: \nLength: " + a + "\nWidth: " + b + "\nHigh: " + c;
 return sentence;
}
}
