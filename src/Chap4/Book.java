//*******************************************************************
// Book.java      Author:sanjin
//
// 定义一个Book构造方法，并获取实例数据和设置方法。
//*******************************************************************

public class Book
{
  private String bookname;
  private String author;
  private String press;
  public String copyrightdate;
  
  public Book(String name1, String name2, String name3, String name4)
{ 
  bookname = name1;
  author = name2;
  press = name3;
  copyrightdate = name4;
}
  public String getBookname()
{
  return bookname;
}
  public String getAuthor()
{
  return author;
}
  public String getPress()
{
  return press;
}
  public String getCopyrightdate()
{
  return copyrightdate;
} 
  public String toString()
{
  return bookname + "\n "+ author + "\n "+ press + "\n" + copyrightdate; 
}
}

     
     

