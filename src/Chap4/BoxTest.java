//*************************************************************************************
//  Box.java           	Author: sanjin
//
//  测试Box类，并返回一些Box对象。
//*************************************************************************************
public class BoxTest
{
 public static void main(String[] args)
 {
  Box box1 = new Box(4,6,7,4);
  Box box2 = new Box(6.6,8.8,9,9);
  System.out.println(box1);
  System.out.println("The box is full." + box1.trunkful());
  System.out.println(box2);
  System.out.println("The box is full." + box2.trunkful());
  box2.setLong(4);
  box2.setWidth(4);
  box2.setHigh(4);
  System.out.println(box2);
  System.out.println("The box is full." + box2.trunkful());
 }
} 
