//*************************************************************************************
//  Flight.java    	Author: snajin
//
//  表示航线名、航班号以及始发地和目的地的城市名等方法。
//*************************************************************************************
public class Flight
{
 public String RouteName; 
 public String RouteNumber;
 public String PlaceOrigin;
 public String Destination;
 
 public Flight(String a, String b, String c, String d)
{
 RouteName = a;
 RouteNumber = b;
 PlaceOrigin = c;
 Destination = d;
}
 public String getRouteName()
{
 return RouteName;
}
 public String getRouteNumber()
{
 return RouteNumber;
}
 public String getPlaceOrigin()
{
 return PlaceOrigin;
}
 public String getDestination()
{
 return Destination;
}
 public void setRouteName(String e)
{
 RouteName = e;
}
 public void setRouteNumber(String f)
{
 RouteNumber = f;
}
 public void setPlaceOrigin(String g)
{
 PlaceOrigin = g;
}
 public void setDestination(String h)
{
 Destination = h;
}
 public String toString()
{
 String result = "Basic flight information: \nRouteName: " + RouteName + "\nRouteNumber: " + RouteNumber + "\nPlaceOrigin: " + PlaceOrigin + "\nDestination: " + Destination;
 return result;
}
}
