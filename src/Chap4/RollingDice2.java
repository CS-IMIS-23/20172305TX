//*************************************************************************************
//  RollingDice2.java    Author: sanjin
//
//  实例化并使用PairOfDice对象。
//*************************************************************************************
public class RollingDice2
{
 public static void main(String[] args)
 {
 PairOfDice twodies = new PairOfDice();
 System.out.println("die1: " + twodies.getValue1() + "\ndie2: " + twodies.getValue2());
 twodies.setValue1(3);
 twodies.setValue2(3);
 System.out.println("die1: " + twodies.getValue1() + "\ndie2: " + twodies.getValue2());
 twodies.rollTwoDies();
 System.out.println("die1's new rolling: " + twodies.getValue1() + "\ndie2's new rolling: " + twodies.getValue2());
 System.out.println("die1 and die2 sum: " + twodies.getTotal());
 }
}
