//*************************************************************************************
//  Dog.java             Author: sanjin
//
//  表示狗的年龄和名字，计算狗的实际年龄的类。
//*************************************************************************************
public class Dog 
{
 private int dold;
 private String dname;
 private int dold1;
 public Dog(String name, int old) 
{
 dold = old; 
 dname = name;
}
 //计算狗的真实年龄
 public int Dtrueold()
{
 dold1 = 7 * dold;
 return dold1; 
}
 //设置狗的年龄
 public void setDold(int old)
{
 dold = old;
}
 //设置狗的名字
 public void setDname(String name)
{
 dname = name;
}
 //获取狗的年龄
 public int getDold()
{
 return dold;
}
 //获取狗的名字
 public String getDname()
{
 return dname;
}
 public String toString()
{
 String result = "Dog's name: " + dname + "\nDog's age: " + dold;
 return result;
}
}
