//*************************************************************************************
//  FlightTest.java            Author: sanjin
//
//  测试FlightTest类的方法。
//*************************************************************************************
public class FlightTest
{
 public static void main(String[] args)
 {
 Flight flight1 = new Flight("a","b","c","d");
 flight1.setRouteName("Beijing Airport");
 flight1.setRouteNumber("HU7630");
 flight1.setPlaceOrigin("Beijing");
 flight1.setDestination("Shanghai");
 Flight flight2 = new Flight("Shanghai Airport","FM9106","Beijing","London");
 System.out.println(flight1);
 System.out.println();
 System.out.println(flight2);
 }
}

