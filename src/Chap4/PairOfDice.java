//*************************************************************************************
//   PairOfDice.java               Author:sanjin
//
//   设置和获取每一个骰子的值的方法，一个掷骰子的方法及一个返回当前两个骰子和的值。
//*************************************************************************************
public class PairOfDice
{
 Die die1 = new Die();
 Die die2 = new Die();
 int sum, value1, value2; 
 public PairOfDice()
{
 sum = die1.getFaceValue() + die2.getFaceValue();
 value1 = die1.roll();
 value2 = die2.roll();
}
 //修改骰子1的值
 public void setValue1(int value1Alt)
{
 die1.setFaceValue(value1Alt);
 value1 = die1.getFaceValue();
}
 //修改骰子2的值
 public void setValue2(int value2Alt)
{
 die2.setFaceValue(value2Alt);
 value2 = die2.getFaceValue();
}
 //访问骰子1的值
 public int getValue1()
{
 value1 = die1.getFaceValue();
 return value1;
}
 //访问骰子2的值
 public int getValue2()
{
 value2 = die2.getFaceValue();
 return value2;
}
 //投掷两个骰子
 public void rollTwoDies()
{
 die1.roll();
 die2.roll();
}
 //两个骰子的和
 public int getTotal()
{
 sum = die1.getFaceValue() + die2.getFaceValue();
 return sum;
}
}
