//*************************************************************************************
// CounterTest.java              Author: sanjin
//
// 测试Counter的方法。
//*************************************************************************************
public class CounterTest
{
  public static void main(String[] args)
 {
  Counter counter1 = new Counter();
  Counter counter2 = new Counter();
  System.out.println("counter1 initialise: " + counter1.getCount());
  System.out.println("counter1 increase one: " + counter1.click());
  System.out.println("counter1 reset: " + counter1.reset());
  System.out.println("counter2 initialise: " + counter2.getCount());
  System.out.println("counter2 increase one: " + counter2.click());
  System.out.println("counter2 reset: " + counter2.reset());
 }
}
