//*************************************************************************************
//  Lights.java              Author: sanjin
//
//  mian方法实例化并返回一些Bulb对象。
//*************************************************************************************
public class Lights
{
 public static void main(String[] args)
 {
  Bulb light1 = new Bulb();
  Bulb light2 = new Bulb();
 
  System.out.println("The condition of the lights1: " + light1.toBoolean());
  System.out.println("The condition of the lights2: " + light2.toBoolean());
 
  light1.turnon();
  light2.turnon();
 
  System.out.println("light1 :" + light1.toBoolean());
  System.out.println("light2 :" + light2.toBoolean());
 }
}
