//*************************************************************************************
// Kennel.java                     Author:sanjin
//
// 测试Dog类
//*************************************************************************************
public class Kennel
{
 public static void main(String[] args)
 {
 
  Dog dog1 = new Dog("Siberian Husky", 2);//哈士奇
  Dog dog2 = new Dog("Labrador Retriever", 1);//拉布拉多
  
  System.out.println("Dog's name: " + dog1.getDname() + "\nDog's age: " + dog1.getDold());
  System.out.println("The true age: " + dog1.Dtrueold());
  System.out.println(dog2);
  System.out.println("The true age: " + dog2.Dtrueold());
 }
}    

