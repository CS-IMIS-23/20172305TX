//*************************************************************************************
// Counter.java         Author:sanjin    
// 
// 表示一个检录计数器，用于统计进入房间的人数。
//*************************************************************************************
public class Counter
{
 int a;
 public Counter()
{
 a = 0;
}
//将计数值增加1。 
 public int click()
{
 a += 1;
 return a;
}
//返回当前的计数值。
 public int getCount()
{
 return a;
}
//计数值重新设定为0。
 public int reset()
{
 a = 0;
 return a;
}
 public String toString()
{
 String b = Integer.toString(a);
 return b; 
}
}
