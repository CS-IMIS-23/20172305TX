//************************************************************************************
//  CarTest.java            Author: sanjin           
//  测试Car类，并返回一些对象。 
//************************************************************************************
public class CarTest
{
 public static void main(String[] args)
 {
  Car cartest1 = new Car("Automobili Lamborghini S.p.A." , "Reventon" , 2008);
  Car cartest2 = new Car("Mercedes-Benz" , "280CE", 1970);
  System.out.println(cartest1);
  System.out.println("The car is curio dealer." + cartest1.isAntique());
  System.out.println(cartest2.getManufacturer() + "\t" + cartest2.getModel() + "\t" + cartest2.getFactoryYear());
  System.out.println("The car is curio dealer." + cartest2.isAntique()); 
 }
}
