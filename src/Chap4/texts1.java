//***************************************************************
//  texts1.java     Author:sanjin
//  生成[-10,10)之间的随机数PseudoNumber，并转为二进制和十六进制 
//***************************************************************
import java.util.*;
public class texts1{
    public static void main(String[] args) {
    Random generator = new Random();
    int PseudoNumber;
    String num1, num2;
    //Generating random number
    PseudoNumber = generator.nextInt(20) - 10;
    System.out.println("Random number :" + PseudoNumber);
    // Generating binary number
    num1 = Integer.toBinaryString(PseudoNumber);
    System.out.println("Binary number :" + num1);
    //Generating hexadecimal number
    num2 = Integer.toHexString(PseudoNumber);
    System.out.println("Hexadecimal number :" + num2);
    }
}

