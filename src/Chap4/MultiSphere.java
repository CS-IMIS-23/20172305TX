//*************************************************************************************
// MultiSphere.java             Author: sanjin
//
// 测试Sphere的方法。
//*************************************************************************************
public class MultiSphere
{
 public static void main(String[] args)
 {
  Sphere a = new Sphere(5);
  System.out.println("diameter of ball: " + 5);
  System.out.println("superficial area: " + a.SuperficialArea());
  System.out.println("volume: " + a.Volume());
  System.out.println(a);
 }
}
