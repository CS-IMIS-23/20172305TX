//**************************************************************************************
// Car.java     Author:sanjin
// 编辑一个Car类，并构造相关方法
//**************************************************************************************
public class Car
{
 public String manufacturer;
 public String model;
 public String things;
 public int factoryyear;
 public Car(String name1, String name2, int name3)
{
 manufacturer = name1;
 model = name2;
 factoryyear = name3;
}
 //读取汽车厂商的名称
 public String getManufacturer()
{
 return manufacturer;
}
 //读取汽车型号
 public String getModel()
{
 return model;
}
 //读取汽车出厂年份
 public int getFactoryYear()
{
 return factoryyear;
}
 //判断汽车是否为古董车
 public boolean isAntique()
{
 boolean word;
 if (2018 - factoryyear > 45)
   word = true;
 else
   word = false; 
 return word;
} 
 //以字符串的形式说明汽车
 public String toString()
{
 String sum = manufacturer + "\t" + model + "\t" +  factoryyear;
 return sum;
}
}
