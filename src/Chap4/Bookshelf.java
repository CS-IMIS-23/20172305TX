public class Bookshelf
{
 public static void main(String[] args)
 {
  Book bookshelf = new Book("Gone with the Wind" , "Margaret Mitchell" , "爱奇艺文学出版社" , "2017-05-23");   
  String bookname, author, press, copyrightdate;
   
  bookname = bookshelf.getBookname();
  author = bookshelf.getAuthor();
  press = bookshelf.getPress();
  copyrightdate = bookshelf.getCopyrightdate();

  System.out.println(bookname + "\n" + author + "\n" + press + "\n" + copyrightdate);
  System.out.println(bookshelf);
  }
}
   
