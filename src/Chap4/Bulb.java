//*************************************************************************************
//  Bulb.java               Author: sanjin
//
//  代表一个可以开或关的灯泡。
//*************************************************************************************
public class Bulb
{
 private boolean light;

 public Bulb()
{ 
 light = false;
}
 public boolean turnon()
{
 light = true;
 return light;
}
 public boolean turnoff()
{
 light = false;
 return light;
}
 public boolean toBoolean()
{
 return light;
}
}
