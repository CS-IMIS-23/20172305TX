//*****************************************************************
//  text1.java        Author: sanjin
// 
//  生成[-10,10)之间的随机数PseudoNumber，并转为十六进制形式。
//*****************************************************************
import java.util.*;
public class text1 {
  
 public static void main(String[] args){
  Random generator = new Random();
  float PseudoNumber;
  PseudoNumber = generator.nextFloat() * 20 - 10;
  
  System.out.println("The number: " + PseudoNumber);
  String num = Float.toHexString(PseudoNumber);
  System.out.println("Hexadecimal number: " + num);
  }
}
