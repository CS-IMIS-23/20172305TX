package thirdweek;
/**
 * Customer represents a waiting customer.
 *
 * @author Lewis and Chase
 * @version 4.0
 */
public class Customer {
    private int arrivalTime ,departureTime;
    /**
     * Creates a new customer with the specified arrival time.
     * @param arrives the arrival time
     */
    public Customer(int arrives) {
        this.arrivalTime = arrives;
        this.departureTime = 0;
    }
    /**
     * Returns the arrival time of this customer.
     * @return the arrival time
     */
    public int getArrivalTime() {
        return arrivalTime;
    }
    /**
     * Sets the departure time for this customer.
     * @param departs the departure time
     *
     */
    public void setArrivalTime(int arrivalTime) {
        this.arrivalTime = arrivalTime;
    }
    /**
     * Returns the departure time of this customer.
     * @greturn the departure time
     */
    public int getDepartureTime() {
        return departureTime;
    }
    /**
     * Returns the departure time of this customer.
     * @greturn the departure time
     */
    public void setDepartureTime(int departureTime) {
        this.departureTime = departureTime;
    }
    /**
     * Computes and returns the total time spent by this customer.
     * @return the total customer time
     */
    public int totalTime(){
        return departureTime-arrivalTime;
    }
}
// * @author LbZhang
// * @version 创建时间：2015年11月17日 上午10:11:02
// * @description
// * Customer对象负责跟踪顾客抵达售票口的时间和顾客买票后离开售票口的时间.
// * 于是，顾客买票所花的总时间 就是抵达时间与离开时间之差。
// * 辅助类
