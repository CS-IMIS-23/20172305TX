package thirdweek;

/**
 *  Codes demonstrates the use of queues to encrypt and messages.
 * @author Lewis and Chase
 * @version 4.0
 */
public class Codes {
    /**
     * Encode and decode a message using a key of values stored in a queue.
     */
    public static void main(String[] args) {
        int[] key = {5, 12, -3, 8, -9, 4, 10};
        Integer keyValue;
        String enCoded = "", deCoded = "";
        String message = "All programmers are playWrights and all computers are lousy actors";
        LinkedQueue<Integer> keyQueue1 = new LinkedQueue<>();
        LinkedQueue<Integer> keyQueue2 = new LinkedQueue<>();

        /** load key queue */
        for(int scan = 0; scan < key.length; scan++){
            keyQueue1.enqueue(key[scan]);
            keyQueue2.enqueue(key[scan]);
        }
        /** encode message */
        for(int scan = 0; scan < message.length(); scan++){
            keyValue = keyQueue1.dequeue();
            enCoded += (char)((int)message.charAt(scan) + keyValue.intValue());
            keyQueue1.enqueue(keyValue);
        }
        System.out.println("Encoded Message:\n"+enCoded+"\n");
        /** decode message */
        for(int scan = 0; scan < enCoded.length(); scan++){
            keyValue = keyQueue2.dequeue();
            deCoded += (char)((int)enCoded.charAt(scan) - keyValue.intValue());
            keyQueue2.enqueue(keyValue);
        }
        System.out.println("Decoded Message:\n"+deCoded);
    }
}
