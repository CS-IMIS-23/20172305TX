package thirdweek;

import secondweek.EmptyCollectionException;
import secondweek.LinearNode;

public class LinkedDeque<T> {
    public int count;
    private LinearNode<T> head, tail;

    public LinkedDeque(){
        count = 0;
        head = tail = null;
    }

    public void enqueue(T element,String string){
        if((string.equals("Head")) || (string.equals("Tail"))){
            if(string.equals("Head")){
                LinearNode<T> node = new LinearNode<T>(element);
                if(isEmpty()) {
                    head = node;
                } else {
                    node.setNext(head);
                }
                head = node;
            }else{
                LinearNode<T> node = new LinearNode<T>(element);
                LinearNode<T> temp1 = head;
                if(isEmpty()) {
                    head = node;
                } else {
                    while(temp1.getNext()!= null){
                        temp1 = temp1.getNext();
                    }
                        temp1.setNext(node);
                }
                tail = node;
            }
            count++;
        }
        else{
            System.out.println("Please enter the word of 'Head' or 'Tail'!!");
        }
    }

    public T dequeue(String string) {
        T result = null;
        if((string.equals("Head") || (string.equals("Tail")))){
            if(string.equals("Head")){
                if (isEmpty()) {
                    throw new EmptyCollectionException("deque");
                }
                result = head.getElement();
                head = head.getNext();
            }else{
                if (isEmpty()) {
                    throw new EmptyCollectionException("deque");
                }
                result = tail.getElement();
                LinearNode<T> temp1 = head, temp2 = null;
                int num = 0;
                while(temp1.getNext() != null){
                    temp2 = temp1;
                    temp1 = temp1.getNext();
                    num++;
                }
                if(num == 0) {
                    temp1.setNext(null);
                    tail = temp1;
                } else {
                    temp2.setNext(null);
                    tail = temp2;
                }
            }
            count--;
        }else{
            System.out.println("Please enter the word of 'Head' or 'Tail'!!");
        }
        if(isEmpty()) {
            tail = null;
        }
        return result;
    }


    public T first() {
        if (isEmpty())
            throw new EmptyCollectionException("deque");
        else
            return head.getElement();
    }


    public boolean isEmpty() {
        if(count == 0 )
            return true;
        else
            return false;
    }


    public int size() {
        return count;
    }


    @Override
    public String toString() {
        LinearNode<T> temp = head;
        String result = "";
        while(temp != null)
        {
            result += temp.getElement().toString() + " ";
            temp = temp.getNext();
        }
        return result;
    }
}
