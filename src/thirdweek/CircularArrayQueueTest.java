package thirdweek;

public class CircularArrayQueueTest{
    public static void main(String args[]){
        CircularArrayQueue circularArrayQueue = new CircularArrayQueue();
        circularArrayQueue.enqueue("1.20172305谭鑫");
        circularArrayQueue.enqueue("2.网络空间安全系");
        circularArrayQueue.enqueue("3.2系3班");
        System.out.println(circularArrayQueue.toString());
        System.out.println("队列移除元素：" + circularArrayQueue.dequeue());
        System.out.println("队列前端内容：" + circularArrayQueue.first());
        System.out.println("队列内是否为空：" + circularArrayQueue.isEmpty());
        System.out.println("队列的元素数目：" + circularArrayQueue.size());
        circularArrayQueue.dequeue();
        System.out.println(circularArrayQueue.toString());
        circularArrayQueue.dequeue();
        System.out.println("队列内是否为空：" + circularArrayQueue.isEmpty());
        System.out.println("队列的元素数目：" + circularArrayQueue.size());
    }
}
