package thirdweek;

public class LinkedQueueTest {
    public static void main(String args[]){
        LinkedQueue linkedQueue = new LinkedQueue();
        linkedQueue.enqueue("1.20172305谭鑫");
        linkedQueue.enqueue("2.网络空间安全系");
        linkedQueue.enqueue("3.2系3班");
        System.out.println(linkedQueue.toString());
        System.out.println("队列移除元素：" + linkedQueue.dequeue());
        System.out.println("队列前端内容：" + linkedQueue.first());
        System.out.println("队列内是否为空：" + linkedQueue.isEmpty());
        System.out.println("队列的元素数目：" + linkedQueue.size());
        linkedQueue.dequeue();
        System.out.println(linkedQueue.toString());
        linkedQueue.dequeue();
        System.out.println("队列内是否为空：" + linkedQueue.isEmpty());
        System.out.println("队列的元素数目：" + linkedQueue.size());
    }
}
