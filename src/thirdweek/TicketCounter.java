package thirdweek;

import java.util.LinkedList;
import java.util.Queue;

/**
 * TickerCounter demonstrates the use of queue for simulating a line of customers.
 *
 * @author Lewis and Chase
 * @version 4.0
 */
public class TicketCounter {

    final static int PROCESS = 120;// /处理时间
    final static int MAX_CASHIERS = 10;//最大收银人数
    final static int NUM_CUSTOMERS = 100;

    public static void main(String[] args) {
        Customer customer;
        Queue<Customer> customerQueue = new LinkedList<Customer>();

        int[] cashierTime = new int[MAX_CASHIERS];
        //收银员的时间标记
        int totalTime, averageTime, departs;

        /** process the simulation for various number of cashiers */
        for (int cashiers = 0; cashiers < MAX_CASHIERS; cashiers++) {
            /** set each cashiers time to zero initially */
            for (int count = 0; count < cashiers; count++)
                cashierTime[count] = 0;

            /** load customer queue */
            for (int count = 1; count <= NUM_CUSTOMERS; count++)
                customerQueue.offer(new Customer(count * 15));

            totalTime = 0;//使用的总体时间

            /** process all customers in the queue */
            while (!(customerQueue.isEmpty())) {
                //不同数量的收银员在进行售票的时候处理顾客服务的时间需求
                for (int count = 0; count <= cashiers; count++) {
                    if (!(customerQueue.isEmpty())) {
                        customer = customerQueue.poll();
                        if (customer.getArrivalTime() > cashierTime[count])
                            departs = customer.getArrivalTime() + PROCESS;
                        else
                            departs = cashierTime[count] + PROCESS;

                        // 离开时间的设置
                        customer.setDepartureTime(departs);
                        cashierTime[count] = departs;
                        //每个顾客使用的最总时间
                        totalTime += customer.totalTime();

                    }
                }
            }

            /** output results for this simulation */
            averageTime = totalTime / NUM_CUSTOMERS;
            System.out.println("Number of cashiers: " + (cashiers + 1));
            System.out.println("Average time: " + averageTime + "\n");
        }
    }
}
