package thirdweek;

public class LinkedDequeTest {
    public static void main(String args[]){
        LinkedDeque linkedDeque = new LinkedDeque();
        linkedDeque.enqueue("2.网络空间安全系","Tail");
        linkedDeque.enqueue("1.20172305谭鑫","Head");
        linkedDeque.enqueue("3.2系3班","Tail");
        System.out.println(linkedDeque.toString());
        System.out.println("双端队列（头）移除元素：" + linkedDeque.dequeue("Head"));
        System.out.println("双端队列（尾）移除元素：" + linkedDeque.dequeue("Tail"));
        System.out.println(linkedDeque.toString());
        System.out.println("队列前端内容：" + linkedDeque.first());
        System.out.println("队列是否为空：" + linkedDeque.isEmpty());
        System.out.println("队列元素数目：" + linkedDeque.size());
        linkedDeque.dequeue("Head");
        System.out.println("栈内是否为空：" + linkedDeque.isEmpty());
        System.out.println("栈的元素数目：" + linkedDeque.size());
    }
}
