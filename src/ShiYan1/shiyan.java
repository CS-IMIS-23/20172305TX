package ShiYan1;

import java.io.*;
import java.util.Scanner;
import java.util.StringTokenizer;

public class shiyan {
    public static void main(String args[]) throws IOException {
        Scanner scan = new Scanner(System.in);
        Element element = new Element();
        String another = "y";
        int num, a = 0, number;
        int[] ints;
        while(another.equalsIgnoreCase("y")){
            System.out.print("Enter some number:");
            num = scan.nextInt();
            element.add(num);
            System.out.print("Enter another(y/n)?");
            another = scan.next();
            System.out.println();
        }

        File file = new File("sort.txt");
        Reader reader = new FileReader(file);
        BufferedReader bufferedreader = new BufferedReader(reader);
        StringTokenizer stringTokenizer = new StringTokenizer(bufferedreader.readLine());
        number = stringTokenizer.countTokens();
        ints = new int[number];
        while(stringTokenizer.hasMoreTokens())
        {
            ints[a] = Integer.parseInt(stringTokenizer.nextToken());
            a++;
        }

        System.out.println("原链表：");
        System.out.println("链表内容总量：" + element.sum());
        System.out.println("链表内容：" + element.toString());
        System.out.println("\n使用冒泡排序");
        element.sort();
        element.insert(5,ints[0]);
        System.out.println("在原链表的第五位添加数字" + ints[0]);
        System.out.println("链表内容总量：" + element.sum());
        System.out.println("链表内容：" + element.toString());
        System.out.println("\n使用冒泡排序");
        element.sort();
        element.insert(0,ints[1]);
        System.out.println("在上个链表的第0位添加数字" + ints[1]);
        System.out.println("链表内容总量：" + element.sum());
        System.out.println("链表内容：" + element.toString());
        System.out.println("\n使用冒泡排序");
        element.sort();
        System.out.println("在添加数字之后的链表删除数字" + ints[0]);
        element.delete(ints[0]);
        System.out.println("链表内容总量：" + element.sum());
        System.out.println("链表内容：" + element.toString());
        System.out.println("\n使用冒泡排序");
        element.sort();
    }
}
