package ShiYan1;

public class Element {
    private intNode head;
    private int nTanXin;
    public Element(){
        head = null;
        nTanXin = 0;
    }
    public void add(int element){
        intNode node = new intNode(element);
        intNode temp;
        if (head == null) {
            head = node;
        } else
        {
            temp = head;
            while (temp.next != null) {
                temp = temp.next;
            }
            temp.next = node;
        }
        nTanXin++;
    }
    public void insert(int place, int number){
        intNode node = new intNode(number);
        intNode temp1, temp2;
        if(sum() >= place){
            if(place == 0)
            {
                node.next = head;
                head = node;
            }
            else
            {
                temp1 = head;
                temp2 = head.next;
                for(int a = 1; a < place; a++)
                {
                    temp1 = temp1.next;
                    temp2 = temp2.next;
                }
                if(temp2 != null)
                {
                    node.next = temp2;
                    temp1.next = node;
                }
                else if(temp2 == null)
                {
                    temp1.next = node;
                }
            }
            nTanXin++;
        }
        else{
            System.out.println("在第" + place +"位插入节点有问题！暂未进行添加！请调试好添加位置再进行！");
        }
    }
    public void delete(int number)
    {
        intNode temp = head;
        if(temp.number == number)
        {
            head = temp.next;
        }
        else {
            while (!(temp.next.getNumber() == number))
            {
                temp = temp.next;
            }
            temp.next = temp.next.next;
        }
        nTanXin--;
    }
    public int sum(){
        return nTanXin;
    }
    public void sort(){
        if(head == null || head.next == null) {
            System.out.println("请往链表内添加元素在进行冒泡排序！！");
        }
        intNode temp1 = head, temp2 = null;
        int num = 0;
        while(temp1.next != temp2){
            while(temp1.next != temp2){
                if(temp1.next.getNumber() < temp1.getNumber()){
                    int number = temp1.getNumber();
                    temp1.setNumber(temp1.next.getNumber());
                    temp1.next.setNumber(number);
                }
                temp1 = temp1.next;
                num++;
                intNode temp = head;
                System.out.print("第" + num + "次：（元素总数:" + nTanXin +"）");
                while (temp != null) {
                    System.out.print(temp.getNumber()+" ");
                    temp = temp.next;
                }
                System.out.println();
            }
            temp2 = temp1;
            temp1 = head;
        }
    }
    @Override
    public String toString(){
        String result = "";
        intNode temp = head;
        while(temp != null){
            result += temp.getNumber() + " ";
            temp = temp.next;
        }
        return result;
    }
    private class intNode {
        public int number;
        public intNode next;

        public intNode(int figure) {
            number = figure;
            next = null;
        }
        public int getNumber() {
            return number;
        }
        public void setNumber(int number) {
            this.number = number;
        }
    }
}
