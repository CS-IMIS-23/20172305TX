package Chap13;

public class NumberTest
{
    public static void main(String[] args)
    {
        NumberList num = new NumberList();
        num.add(new Number(2));
        num.add(new Number(6));
        num.add(new Number(1));
        System.out.println(num);

        num.sort();
        System.out.println(num);
    }
}
