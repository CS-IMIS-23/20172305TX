package Chap13;

public class DVDList
{
    private DVDNode list;
    public DVDList()
    {
        list = null;
    }

    public void add(DVD movie)
    {
        DVDNode node = new DVDNode(movie);
        DVDNode current;

        if (list == null)
        {
            list = node;
        }
        else
        {
            current = list;
            while (current.next != null)
            {
                current = current.next;
            }
            current.next = node;
        }
    }
    @Override
    public String toString()
    {
        String result = "";

        DVDNode current = list;

        while (current != null)
        {
            result += current.movie.toString() + "\n";
            current = current.next;
        }

        return result;
    }

    private class DVDNode
    {
        public DVD movie;
        public DVDNode next;

        //--------------------------------------------------------------
        //  Sets up the node
        //--------------------------------------------------------------
        public DVDNode(DVD mag)
        {
            movie = mag;
            next = null;
        }
    }
}