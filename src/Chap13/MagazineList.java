package Chap13;

import Chap10.Sorting;

import java.util.ArrayList;

public class MagazineList
{
    private MagazineNode list;
    public MagazineList()
    {
        list = null;
    }

    public void add(Magazine mag)
    {
        MagazineNode node = new MagazineNode(mag);
        MagazineNode current;

        if (list == null)
            list = node;
        else
        {
            current = list;
            while (current.next != null)
                current = current.next;
                current.next = node;
        }
    }

    public void insert(int index, Magazine newMagazine)
    {
        MagazineNode node = new MagazineNode(newMagazine);
        MagazineNode current1, current2;

        int n = Int(list);
        if(index <=  n + 1)
        {
            if(index == 1)
            {
                node.next = list;
                list = node;
            }
            else
            {
                current2 = list;
                current1 = list.next;
                for(int a = 1; a < index - 1; a++)
                {
                    current2 = current2.next;
                    current1 = current1.next;
                }
                if(current1 != null)
                {
                    node.next = current1;
                    current2.next = node;
                }
                else if(current1 == null)
                {
                    current2.next = node;
                }
            }
        }
        else
            System.out.println("插入节点有问题！！！未进行添加！！！");
    }

    private int Int(MagazineNode list)
    {
        int a = 1;
        while(list.next != null)
        {
            list = list.next;
            a++;
        }
        return a;
    }

    public void delete(Magazine Node)
    {
        MagazineNode current1 = list;

        if(String.valueOf(current1.magazine).equals(String.valueOf(Node)))
        {
            list = current1.next;
        }
        else {
            while (!(String.valueOf(current1.next.magazine).equals(String.valueOf(Node))))
            {
                current1 = current1.next;
            }
            current1.next = current1.next.next;
        }
    }

    public String toString()
    {
        String result = "";

        MagazineNode current = list;

        while (current != null)
        {
            result += current.magazine + "\n";
            current = current.next;
        }

        return result;
    }

    private class MagazineNode
    {
        public Magazine magazine;
        public MagazineNode next;

        public MagazineNode(Magazine mag)
        {
            magazine = mag;
            next = null;
        }
    }

    public void Sorting()
    {
        int n =0;
        ArrayList<Comparable> magazines = new ArrayList();
        while (list!=null)
        {
            magazines.add(n, list.magazine);
            list = list.next;
            n++;
        }
        n = 0;
        Comparable[] list = new Comparable[magazines.size()];
        while (n<magazines.size())
        {
            list[n] = magazines.get(n);
            n++;
        }
        Sorting.selectionSort1(list);
        for (Comparable com : list)
            System.out.println(com);
    }
}
