package Chap13;

import java.rmi.MarshalledObject;

public class movie
{
    public static void main(String[] args)
    {
        DVDList movie = new DVDList();

        movie.add(new DVD("The Godfather", "Francis Ford Coppola", 1972, 24.95, true));
        movie.add(new DVD("District 9", "Neill Blomkamp", 2009, 19.95, false));
        movie.add(new DVD("Iron Man","Jon Favreau", 2008, 15.95, false));
        movie.add(new DVD("All About Eve", "Joseph Mankiewicz", 1950, 17.50, false));
        movie.add(new DVD("The Matrix", "Andy & Lana Wachowski", 1999, 19.95, true));
        movie.add(new DVD("Iron Man 2","Jon Favreau", 2010, 22.99, false));
        movie.add(new DVD("Casablanca","Michael Curtiz", 1942, 19.95, false));
        System.out.println(movie);
    }
}
