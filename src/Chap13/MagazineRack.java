package Chap13;
//*******************************************************************
//  MagazineRack.java       Author: Lewis/Loftus
//
//  Driver to exercise the MagazineList collection.
//*******************************************************************

public class MagazineRack
{
   //----------------------------------------------------------------
   //  Creates a MagazineList object, adds several magazines to the
   //  list, then prints it.
   //----------------------------------------------------------------
   public static void main(String[] args)
   {    
      MagazineList rack = new MagazineList();
      
      rack.add(new Magazine("Time"));
      rack.add(new Magazine("Woodworking Today"));
      rack.add(new Magazine("Communications of the ACM"));
      rack.add(new Magazine("House and Garden"));
      rack.add(new Magazine("GQ"));
      
      System.out.println(rack); 

      rack.insert(1,new Magazine("***********"));
      System.out.println(rack);

      rack.insert(7,new Magazine("121321432412421"));
      System.out.println(rack);

      rack.insert(10,new Magazine("121321"));
      System.out.println(rack);

      rack.delete(new Magazine("Communications of the ACM"));
      System.out.println(rack);

      rack.delete(new Magazine("***********"));
      System.out.println(rack);

      rack.Sorting();
      System.out.println(rack);
   }
}
