package Chap13;
//*************************************************************************************
//  DVD.java   		Author: sanjin
//
//  Represents  a DVD video disc.
//*******************************************************************************

import java.text.NumberFormat;

public class DVD
{
  private String title, director;
  private int year;
  private double cost;
  private boolean bluray;

  //-----------------------------------------------------------------------------------------
  //  Creates a new DVD with the specified information.
  //-----------------------------------------------------------------------------------------
  public DVD(String title, String director, int year, double cost, boolean bluray)
  {
    this.title = title;
    this.director = director;
    this.year = year;
    this.cost = cost;
    this.bluray = bluray;
  } 

  //---------------------------------------------------------------------------------------------
  //  Returns a string description of this DVD.
  //---------------------------------------------------------------------------------------------
  @Override
  public String toString()
  {
    NumberFormat fmt = NumberFormat.getCurrencyInstance();
    String description;

    description = title + "\t" + year +"\t";
    description += director + "\t" + fmt.format(cost);
    if (bluray) {
      description += "\t" + "Blu-ray";
    }
    return description;
  } 
}
