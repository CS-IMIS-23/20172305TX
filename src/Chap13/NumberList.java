package Chap13;

public class NumberList
{
    private NumNode list;

    public NumberList()
    {
        list = null;
    }

    public void add(Number num)
    {
        NumNode node = new NumNode(num);
        NumNode current;
        if (list == null)
        {
            list = node;
        }
        else
            {
                current = list;
                while (current.next != null)
                {
                    current = current.next;
                }
                current.next = node;
            }
        }
    public void sort()
    {
        NumNode min;
        Number temp;
        NumNode numNode = list;

        while (numNode != null)
        {
            min = numNode;
            NumNode current = min.next;
            while (current!=null)
            {
                if(Integer.parseInt(String.valueOf(min.number)) > Integer.parseInt(String.valueOf(current.number)))
                {
                    min = current ;
                }
                current = current.next;
            }
            temp = min.number;
            min.number = numNode.number;
            numNode.number = temp;

            numNode  = numNode.next;
        }
    }

    @Override
    public String toString()
    {
        String result = "";

         NumNode current = list;
         while (current != null)
         {
             result += current.number + " ";
             current = current.next;
         }
            return result;
        }

    private class NumNode
    {
        public Number number;
        public NumNode next;

         public NumNode(Number number)
         {
             this.number = number;
             next = null;
         }
    }
}

