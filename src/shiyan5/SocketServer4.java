package shiyan5;

import Calculate.Judge;
import Practice.DH.KeyAgree;
import Practice.DH.Key_DH;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.Key;

public class SocketServer4 {
    public static void main(String[] args) throws Exception{
            System.out.println("start...");
            ServerSocket serversocket = new ServerSocket(8800);
            Socket socketOnServer = serversocket.accept();
            //数据输出流
            DataOutputStream dataOutputStream = new DataOutputStream(socketOnServer.getOutputStream());
            //数据输入流
            DataInputStream dataInputStream = new DataInputStream(socketOnServer.getInputStream());
            //公钥与私钥，在Key_DH。creat方法下创建DH1pub.txt和DH1pri.txt两个文件，一个存放公钥，一个存放私钥
            Key_DH.creat("DH1pub.txt", "DH1pri.txt");
            int num = Integer.parseInt(dataInputStream.readUTF());
            byte np[] = new byte[num];
            for (int i = 0;i<num;i++) {
                String temp = dataInputStream.readUTF();
                np[i] = Byte.parseByte(temp);
            }
            ObjectInputStream objectInputStream = new ObjectInputStream (new ByteArrayInputStream (np));
            Key key1 = (Key)objectInputStream.readObject();
            FileOutputStream fileOutputStream1 = new FileOutputStream("Cpub.txt");
            ObjectOutputStream objectOutputStream1 = new ObjectOutputStream(fileOutputStream1);
            objectOutputStream1.writeObject(key1);
            FileInputStream fileInputStream = new FileInputStream("DH1pub.txt");
            ObjectInputStream bp = new ObjectInputStream(fileInputStream);
            Key key2 = (Key) bp.readObject();//DH1pub.txt中读取内容，并转化为Key类型的
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ObjectOutputStream objectOutputStream2 = new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream2.writeObject(key2);
            byte[] str = byteArrayOutputStream.toByteArray();
            dataOutputStream.writeUTF(str.length + "");
            for (int i = 0; i < str.length; i++) {
                dataOutputStream.writeUTF(str[i] + "");
            }
            KeyAgree.creat("Cpub.txt", "DH1pri.txt");
            String string = dataInputStream.readUTF();
            byte ctext[] = new byte[Integer.parseInt(string)];
            for (int i = 0;i<Integer.parseInt(string);i++) {
                String temp = dataInputStream.readUTF();
                ctext[i] = Byte.parseByte(temp);
            }
            // 获取密钥
            FileInputStream f = new FileInputStream("miyao.txt");
            byte[] keysb = new byte[24];
            f.read(keysb);
            System.out.print("最终密钥：");
            for (int i = 0;i<24;i++) {
                System.out.print(keysb[i]+" ");
            }
            System.out.println();
            SecretKeySpec k = new SecretKeySpec(keysb, "DESede");
            //密文
            Cipher cp = Cipher.getInstance("DESede");
            cp.init(Cipher.DECRYPT_MODE, k);
            byte[] ptext = cp.doFinal(ctext);
            System.out.print("后缀表达式的字节数组：");
            for (int i = 0; i < ptext.length; i++) {
                System.out.print(ptext[i] + " ");
            }
            System.out.println();
            //解密后的明文
            String p = new String(ptext, "UTF8");
            System.out.println("服务器解密后的后缀表达式：" + p);
            //产生最后结果，并把结果传到客户端上
            Judge judge = new Judge();
            dataOutputStream.writeUTF(judge.answer(p));
            System.out.println("服务器计算结果：" + judge.answer(p));
    }
}
