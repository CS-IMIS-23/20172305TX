package shiyan5;

import Calculate.SuffixExpression;
import Practice.DH.KeyAgree;
import Practice.DH.Key_DH;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.net.Socket;
import java.security.Key;
import java.util.Scanner;

public class SocketClient4 {
    public static void main(String args[]) throws Exception {
            System.out.println("start...");
            Scanner scanner = new Scanner(System.in);
            Socket socket = new Socket("192.168.43.40", 8800);
            //数据输入流
            DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
            //数据输出流
            DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
            //产生中缀表达式和后缀表达式
            System.out.print("请输入中缀表达式：");
            String zhongzhui = scanner.nextLine();
            SuffixExpression suffixExpression = new SuffixExpression();
            String houzhui = suffixExpression.SuffixExpression(zhongzhui);
            System.out.println("产生的后缀表达式：" + houzhui);
            //公钥与私钥，在Key_DH。creat方法下创建DH2pub.txt和DH2pri.txt两个文件，一个存放公钥，一个存放私钥
            Key_DH.creat("DH2pub.txt","DH2pri.txt");
            FileInputStream fileInputStream = new FileInputStream("DH2pub.txt");
            ObjectInputStream objectInputStream1 = new ObjectInputStream(fileInputStream);
            Key key1 = (Key) objectInputStream1.readObject();//DH2pub.txt中读取内容，并转化为Key类型的
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ObjectOutputStream objectOutputStream1 = new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream1.writeObject(key1);
            byte[] kb = byteArrayOutputStream.toByteArray();
            dataOutputStream.writeUTF(kb.length + "");//使用 UTF-8 修改版编码将一个字符串写入基础输出流
            for (int i = 0; i < kb.length; i++) {
                dataOutputStream.writeUTF(kb[i] + "");
            }
            int num = Integer.parseInt(dataInputStream.readUTF());
            byte str[] = new byte[num];
            for (int i = 0;i<num;i++) {
                String temp = dataInputStream.readUTF();
                str[i] = Byte.parseByte(temp);
            }
            ObjectInputStream objectInputStream2 = new ObjectInputStream (new ByteArrayInputStream (str));
            Key key2 = (Key)objectInputStream2.readObject();;
            FileOutputStream fileOutputStream = new FileOutputStream("DH1pub.txt");
            ObjectOutputStream objectOutputStream2 = new ObjectOutputStream(fileOutputStream);
            objectOutputStream2.writeObject(key2);
            //通过KeyAgree.creat的方法产生DH1pub.txt和DH2pri.txt，对方公钥和自己的私钥产生的终极密钥miyao.txt
            KeyAgree.creat("DH1pub.txt","DH2pri.txt");
            FileInputStream fileInputStreamm = new FileInputStream("miyao.txt");
            byte[] keysb = new byte[24];
            fileInputStreamm.read(keysb);
            //输出终极密钥
            System.out.print("最终密钥：");
            for (int n = 0;n<24;n++) {
                System.out.print(keysb[n]+" ");
            }
            System.out.println();
            SecretKeySpec k = new SecretKeySpec(keysb, "DESede");
            Cipher cp = Cipher.getInstance("DESede");
            cp.init(Cipher.ENCRYPT_MODE, k);
            byte ptext[] = houzhui.getBytes("UTF8");
            //产生密文
            byte ctext[] = cp.doFinal(ptext);
            System.out.print("加密的后缀表达式：");
            for (int a = 0; a < ctext.length; a++) {
                System.out.print(ctext[a] + " ");
            }
            System.out.println();
            dataOutputStream.writeUTF(ctext.length + "");
            for (int b = 0; b < ctext.length; b++) {
                dataOutputStream.writeUTF(ctext[b] + "");
            }
            //接收服务器的传来的内容
            String answer = dataInputStream.readUTF();
            System.out.println("服务器的答案：" + answer);
    }
}
