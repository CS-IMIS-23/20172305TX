package shiyan5;

import Calculate.Judge;
import Calculate.SuffixExpression;

public class MyDCTest {
    public static void main(String[] args){
        SuffixExpression suffixExpression = new SuffixExpression();
        Judge judge = new Judge();

        String timu = "2 + 45 - 64 * 2 ÷ 34 = ";
        System.out.println("中缀表达式：" + timu);
        String houzhui = suffixExpression.SuffixExpression(timu);
        System.out.println("后缀表达式：" + houzhui);
        String deshu = judge.answer(houzhui);
        System.out.println("结果：" + deshu);
    }
}
