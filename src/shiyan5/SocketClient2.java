package shiyan5;

import Calculate.SuffixExpression;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class SocketClient2 {
    public static void main(String[] args) throws IOException {
       //1.建立客户端Socket连接，指定服务器位置和端口
       // Socket socket = new Socket("localhost",8080);
       Socket socket = new Socket("192.168.70.128",8800);
       Scanner scan = new Scanner(System.in);
       //2.得到socket读写流
       OutputStream outputStream = socket.getOutputStream();
       //  PrintWriter printWriter = new PrintWriter(outputStream);
       OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
       //输入流
       InputStream inputStream = socket.getInputStream();
       BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"UTF-8"));
       //3.利用流按照一定的操作，对socket进行读写操作

       System.out.print("请输入您的中缀表达式：");
       String nifixexpression = scan.nextLine();
       //     String info = new String(info1.getBytes("utf-8"));
       //     printWriter.write(info);
       //     printWriter.flush();
       SuffixExpression suffixexpression = new SuffixExpression();
       String houzhui = suffixexpression.SuffixExpression(nifixexpression);
       outputStreamWriter.write(houzhui);
       outputStreamWriter.flush();
       socket.shutdownOutput();
       //接收服务器的响应
       System.out.println("传输信息为：" + houzhui);
       String reply = null;
       while (!((reply = bufferedReader.readLine()) ==null)){
           System.out.println("接收来自虚拟机服务器的信息为：" + reply);
       }

       //4.关闭资源
       bufferedReader.close();
       inputStream.close();
       outputStreamWriter.close();
       //printWriter.close();
       outputStream.close();
       socket.close();
    }
}
