package shiyan5;

import Calculate.SuffixExpression;
import Practice.DH.KeyAgreee;
import Practice.MD5.DigestPass;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class SocketClient7 {
    public static void main(String[] args) throws IOException,ClassNotFoundException,IOException,Exception{
        System.out.println("start...");
        Scanner scan = new Scanner(System.in);
        Socket socket = new Socket("172.16.43.187", 8800);
        OutputStream os = socket.getOutputStream();
        //用于接收服务器发来的数据的输入流对象
        ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
        //用于向服务器发送数据的输出流对象
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
        //产生中缀表达式和后缀表达式
        SuffixExpression suffixExpression = new SuffixExpression();
        System.out.print("请输入中缀表达式：");
        String zhongzhui = scan.nextLine();
        String houzhui = suffixExpression.SuffixExpression(zhongzhui);
        System.out.println("传输信息：" + houzhui);
        //通过KeyAgreee.getPassWord方法产生DHpbk.dat和DHprk.dat两个密钥文件中读取密文
        KeyAgreee keyAgreee = new KeyAgreee();
        DigestPass digestPass = new DigestPass();
        String password = keyAgreee.getPassWord("DHpbk.dat","DHprk.dat");
        //产生的消息摘要
        String md5 = digestPass.getMD5(houzhui);
        System.out.println("MD5值："+ md5);
        //DH算法产生的密钥
        System.out.println("加密后的密文："+ password);
        byte [] byteContent = AES.encrypt(houzhui,password);//在AES类中，把后缀和密钥生成一个字节数组
        //内容传到服务器
        objectOutputStream.writeObject(byteContent);
        objectOutputStream.writeObject(md5);
        // 接收服务器内容
        String result = (String) objectInputStream.readObject();
        System.out.println("答案：" + result + "\n");
    }
}
