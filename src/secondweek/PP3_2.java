package secondweek;

import java.util.Scanner;

public class PP3_2 {
    public static void main(String args[]){
        String another = "y";
        Scanner scan = new Scanner(System.in);

        while(another.equalsIgnoreCase("y")) {
            System.out.print("Enter a sentence:");

             String sentence1 = scan.nextLine();

            Character[] Char = new Character[sentence1.length()];
            for (int a = 0; a < sentence1.length(); a++) {
                Char[a] = (sentence1.charAt(a));
            }

            int num = 0;
            String sentence2 = "";
            while (num < sentence1.length()) {
                sentence2 += Char[sentence1.length() - num - 1];
                num++;
            }
            System.out.println("The reverse sentence:" + sentence2);

            System.out.print("Enter another(y/n)?");
            another = scan.nextLine();
            System.out.println();
        }
    }
}
