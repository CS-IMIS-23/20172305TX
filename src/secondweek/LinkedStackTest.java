package secondweek;

public class LinkedStackTest {
    public static void main(String args[]){
        LinkedStack linkedStack = new LinkedStack();
        linkedStack.push("1.20172305谭鑫");
        linkedStack.push("2.网络空间安全系");
        linkedStack.push("3.2系3班");
        System.out.println(linkedStack.toString());
        System.out.println("栈移除元素：" + linkedStack.pop());
        System.out.println("栈顶内容：" + linkedStack.peek());
        System.out.println("栈内是否为空：" + linkedStack.isEmpty());
        System.out.println("栈的元素数目：" + linkedStack.size());
        linkedStack.pop();
        System.out.println(linkedStack.toString());
        linkedStack.pop();
        System.out.println("栈内是否为空：" + linkedStack.isEmpty());
        System.out.println("栈的元素数目：" + linkedStack.size());
    }
}
