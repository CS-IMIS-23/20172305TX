package secondweek;

public class ArrayStackTest
{
    public static void main(String[] args){
        ArrayStack arrayStack = new ArrayStack();
        //System.out.println(arrayStack.peek());
        arrayStack.push("1.20172305谭鑫");
        arrayStack.push("2.网络空间安全系");
        arrayStack.push("3.2系3班");
        System.out.println(arrayStack.toString());
        System.out.println("栈移除元素：" + arrayStack.pop());
        System.out.println("栈顶内容：" + arrayStack.peek());
        System.out.println("栈内是否为空：" + arrayStack.isEmpty());
        System.out.println("栈的元素数目：" + arrayStack.size());
        arrayStack.pop();
        System.out.println(arrayStack.toString());
        arrayStack.pop();
        System.out.println("栈内是否为空：" + arrayStack.isEmpty());
        System.out.println("栈的元素数目：" + arrayStack.size());

    }
}
