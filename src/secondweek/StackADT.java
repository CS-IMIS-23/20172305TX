package secondweek;
/**
 * Defines the interface to a stack collection.
 * @author Lewis and Chase
 * @version 4.0
 */
public interface StackADT<T> {
    /**
     * Adds the specified element to the top of this stack.
     * @param element element to be pushed onto the stack
     */
     public void push(T element);
    /**
     * Removes and returns the top element from this stack.
     * @return the element removed from the stack
     */
    public T pop();
    /**
     * Return without removing the top element of this stack.
     * @return the element removed from the stack
     */
    public T peek();
    /**
     * Return true if this stack contains no elements..
     * @return true if the stack is empty
     */
    public boolean isEmpty();
    /**
     * Return the number of elements in this stack.
     * @return the number of elements in the stack
     */
    public int size();
    /**
     * Return a string representation of this satck.
     * @return a string representation of the stack
     */
    @Override
    public String toString();
}
