package secondweek;

public class Number {
    private intNode head;
    public Number(){
        head = null;
   }
    public void insertNumber(int num)
    {
        intNode node = new intNode(num);
        intNode temp;

        if (head == null) {
            head = node;
        } else
        {
            temp = head;
            while (temp.next != null) {
                temp = temp.next;
            }
            temp.next = node;
        }
    }
   public void insertNumber(int weizhi, int num){
        intNode node = new intNode(num);
        intNode temp1, temp2;
            //中间插
            if(times(head) + 1 >= weizhi){

                if(weizhi == 1)
                {
                    node.next = head;
                    head = node;
                }
                else
                {
                    temp1 = head;
                    temp2 = head.next;
                    for(int a = 1; a < weizhi - 1; a++)
                    {
                                 temp1 = temp1.next;
                    temp2 = temp2.next;
                }
                if(temp2 != null)
                {
                    node.next = temp2;
                    temp1.next = node;
                }
                //尾插法
                else if(temp2 == null)
                {
                    temp1.next = node;
                }
                }
            }
            else{
                System.out.println("在第" + weizhi +"位插入节点有问题！暂未进行添加！请调试好添加位置再进行！");
            }

   }
   public void deleteNumber(int weizhi,int num){
       intNode node = new intNode(num);
       intNode temp1, temp2;
       //头删除
       if((weizhi == 1)&&(head.number == num)){
           head = head.next;
       }
       else{
           if(weizhi <= times(head) + 1){
               temp1 = head;
               temp2 = head.next;
               for(int a = 1; a < weizhi - 1; a++)
               {
                   temp1 = temp1.next;
                   temp2 = temp2.next;
               }
               //中间删除
               if(temp2.number == node.number){
                   //中间删除
                   if(temp2.next != null){
                       temp1.next = temp2.next;
                   }
                   //尾删除
                   else{
                       temp1.next = null;
                   }
               }
               else{
                   System.out.println("在第" + weizhi +"位删除节点有问题！暂未进行删除！请调试好删除位置再进行！");
               }
           }else{
               System.out.println("在第" + weizhi +"位删除节点有问题！暂未进行删除！请调试好删除位置再进行！");
           }
       }
   }
    public void deleteNumber(int num)
    {
        intNode temp = head;

        if(temp.number == num)
        {
            head = temp.next;
        }
        else {
            while (!(temp.next.number == num))
            {
                temp = temp.next;
            }
            temp.next = temp.next.next;
        }
    }
    @Override
    public String toString() {
        String result = "";
        intNode temp = head;
        while(temp != null){
            result += temp.number + " ";
            temp = temp.next;
        }
        return result;
    }
    private int times(intNode list){
        int num = 1;
        while(list.next != null){
            list = list.next;
            num++;
        }
        return num;
   }
   public void sorting(){
       intNode min;
       int temp;
       intNode numNode = head;
       while (numNode!=null)
       {
           min = numNode;
           intNode current = min.next;
           while (current!=null)
           {
               if(current.number>min.number) {
                   min = current ;
               }
               current = current.next;
           }
           temp= min.number;
           min.number = numNode.number;
           numNode.number = temp;

           numNode  = numNode.next;
       }
   }
    private class intNode{
        public int number;
        public intNode next;
        public intNode(int figure){
            number = figure;
            next = null;
        }
   }
}