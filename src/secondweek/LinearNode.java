package secondweek;
/*
* Represents a node in a linked list.
*
* @author Lewis and Chase
* @version 4.0
*/
public class LinearNode<T> {
    private LinearNode<T> next;
    private T element;
    /**
     *
     * Create an empty node.
    */
    public LinearNode(){
        next = null;
        element = null;
    }
    /**
     * Creats a node storing the specified element.
     * @param elem element to be stored
     */
    public LinearNode (T elem) {
        next=null;
        element = elem;
    }
    /**
     * Returns the node that follows this one.
     * @return LinearNode<T> reference to next node
     */
    public LinearNode<T> getNext(){
        return next;
    }
    /**
     * Sets the node that follows this one.
     * @param node nnode to follow this one
     */
    public void setNext(LinearNode<T> node)
    {
        next=node;
    }
    /**
     * Returns the element stored in this node.
     * @return  T element stored at the node.
     */
    public T getElement(){
        return element;
    }
    /**
     * Sets the element stored in the node.
     * @param elem element to be stored at thhis node
     */
    public void setElement(T elem)
    {
        element= elem;
    }
}
