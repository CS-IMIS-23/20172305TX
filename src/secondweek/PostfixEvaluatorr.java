package secondweek;

import java.util.Scanner;

public class PostfixEvaluatorr {
    private final static char ADD = '+';
    private final static char SUBTRACT = '-';
    private final static char MULTIPLY = '*';
    private final static char DIVIDE = '/';

    private Number number = new Number();
    public PostfixEvaluatorr() {

    }
    public int evaluate(String expr){
        int op1, op2, result = 0;
        String token;
        Scanner parser = new Scanner(expr);

        while(parser.hasNext()){
            token = parser.next();

            if(isOperator(token)){
                op2 = (number.pop());
                op1 = (number.pop());
                result = evaluateSingleOperator(token.charAt(0), op1,op2);
            }else {
                number.push(Integer.parseInt(token));
            }
        }
        return result;
    }
    private boolean isOperator(String token){
        return(token.equals("+") || token.equals("-") || token.equals("*") || token.equals("/"));
    }
    private int evaluateSingleOperator(char operator, int op1, int op2){
        int result = 0;

        switch (operator){
            case ADD:
                result = op1 + op2;
                break;
            case SUBTRACT:
                result = op1 - op2;
                break;
            case MULTIPLY:
                result = op1 * op2;
                break;
            case DIVIDE:
                result = op1 / op2;
        }
        return result;
    }
    public class Number{
        int count = 0;
        private intNode head;
        public void push(int number){
            intNode temp = new intNode(number);
            temp.setNext(head);
            head = temp;
            count++;
        }
        public int pop() throws EmptyCollectionException{
            if(isEmpty()) {
                throw new EmptyCollectionException("StackADT");
            }
            int result = head.getNumber();
            head = head.getNext();
            count--;
            return result;
        }
        public boolean isEmpty(){
            if(count == 0){
                return true;
            }
            else {
                return false;
            }
        }
    }
    private class intNode{
        public int number;
        public intNode next;
        public intNode(int figure){
            number = figure;
            next = null;
        }
        public intNode getNext(){
            return next;
        }
        public void setNext(intNode node){
            next = node;
        }
        public int getNumber(){
            return number;
        }
        public void setNext(int number){
            this.number = number;
        }
    }
}
//public class Number{
//        int count = 0;
//        private intNode head;
//        public Number(){
//            head = null;
//        }
//        public void insertNumber(int num)
//        {
//            intNode node = new intNode(num);
//            intNode temp;
//
//            if (head == null) {
//                head = node;
//            } else
//            {
//                temp = head;
//                while (temp.next != null) {
//                    temp = temp.next;
//                }
//                temp.next = node;
//            }
//        }
//        public int deleteNumber(){
//            if (isEmpty()) {
//                throw new EmptyCollectionException("StackADT");
//            }
//            int result = head.number;
//            head = head.next;
//            count--;
//            return result;
//        }
//        public boolean isEmpty(){
//            if(count == 0){
//                return true;
//            }
//            else {
//                return false;
//            }
//        }
//        private class intNode{
//            public int number;
//            public intNode next;
//            public intNode(int figure){
//                number = figure;
//                next = null;
//            }
//        }
//    }