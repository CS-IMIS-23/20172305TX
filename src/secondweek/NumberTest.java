package secondweek;

import java.util.Scanner;
import java.util.StringTokenizer;

public class NumberTest {
    public static void main(String args[]){
        Number number = new Number();
        Scanner scan = new Scanner(System.in);
        System.out.print("链表内添加内容：");
        String lianbiao = scan.nextLine();
        StringTokenizer lian = new StringTokenizer(lianbiao, " ");
        while (lian.hasMoreTokens())
        {
            number.insertNumber(Integer.parseInt(lian.nextToken()));
        }
        System.out.println("形成链表：");
        System.out.println(number.toString() + "\n----------------------------");

        System.out.println("在链表内插入内容(在1号位置添加2)：");
        number.insertNumber(1,2);
        System.out.println(number.toString() + "\n");
        System.out.println("在链表内插入内容(在3号位置添加3)：");
        number.insertNumber(3,3);
        System.out.println(number.toString() + "\n");
        System.out.println("在链表内插入内容(在2号位置添加447)：");
        number.insertNumber(2,447);
        System.out.println(number.toString() + "\n");
        System.out.println("在链表内插入内容(在1号位置添加234)：");
        number.insertNumber(1,234);
        System.out.println(number.toString() + "\n");
        System.out.println("在链表内插入内容(在19号位置添加234)：");
        number.insertNumber(19,234);
        System.out.println(number.toString() + "\n----------------------------");

        System.out.println("在链表内删除内容(在1号位置删除23)：");
        number.deleteNumber(1,23);
        System.out.println(number.toString() + "\n");
        System.out.println("在链表内删除内容(在8号位置删除447)：");
        number.deleteNumber(8,447);
        System.out.println(number.toString() + "\n");
        System.out.println("在链表内删除内容(删除234)：");
        number.deleteNumber(234);
        System.out.println(number.toString() + "\n----------------------------");

        System.out.println("对链表进行排序（排序前）：" + number.toString());
        number.sorting();
        System.out.println("对链表进行排序（排序后）：" + number.toString());;
    }
}
