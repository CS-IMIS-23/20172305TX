package secondweek;

import java.util.Arrays;

public class ArrayStack<T>
{
    private final int DEFAULT_CAPACITY = 100;
    private int top;
    private T[] stack;
    public ArrayStack(){
        top = 0;
        stack = (T [])(new Object[DEFAULT_CAPACITY]);
    }
    public void push(T element){
        if (size() == stack.length) {
            expandCapacity();
        }
            stack[top] = element;
        top++;
    }
    private void expandCapacity(){
        stack = Arrays.copyOf(stack,stack.length * 2);
    }
    public Object pop() throws EmptyCollectionException{
        if(isEmpty()) {
            throw new EmptyCollectionException("Stack");
        }
        top--;
        Object result = stack[top];
        stack[top] = null;
        return result;
    }
    public T peek() throws EmptyCollectionException
    {
        if(isEmpty()){
            throw new EmptyCollectionException("Stack");
        }
        return stack[top - 1];
    }
    public boolean isEmpty()
    {
        // To be completed as a Programming Project
        if(top == 0){
            return true;
        }
        else{
            return false;
        }
    }
    public int size()
    {
        // To be completed as a Programming Project
        return top;
    }
    @Override
    public String toString()
    {
        // To be completed as a Programming Project
        String result = "";
        for(int num = size() - 1 ;num >= 0; num--) {
            result += stack[num] +" ";
        }
        return result;
    }
}
