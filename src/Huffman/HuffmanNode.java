package Huffman;

public class HuffmanNode<T> implements Comparable<HuffmanNode<T>>{
    private T letter;
    private double weight;
    private HuffmanNode lChild, rChild;
    private String code;
    public HuffmanNode(T letter, double weight){
        this.letter = letter;
        this.weight = weight;
        code = "";
    }

    public T getLetter() {
        return letter;
    }

    public double getWeight() {
        return weight;
    }

    public HuffmanNode getlChild() {
        return lChild;
    }

    public HuffmanNode getrChild() {
        return rChild;
    }

    public String getCode() {
        return code;
    }

    public void setLetter(T letter) {
        this.letter = letter;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setlChild(HuffmanNode lChild) {
        this.lChild = lChild;
    }

    public void setrChild(HuffmanNode rChild) {
        this.rChild = rChild;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "Huffman: " + letter + " 权值:" + weight + "编码:" + code;
    }

    @Override
    public int compareTo(HuffmanNode<T> huffmanNode) {
        if(this.weight > huffmanNode.getWeight())
            return -1;
        else
            return 1;
    }
}
