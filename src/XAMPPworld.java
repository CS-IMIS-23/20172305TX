import java.sql.*;

public class XAMPPworld
{
    public static void main(String[] args) {
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + "world?user=root&password=");
            if (conn != null) {
                System.out.println("We have connected to our database!");

                Statement stmt1 = conn.createStatement();
                ResultSet rset1 = stmt1.executeQuery("SELECT * FROM city WHERE city.Population>7017230");
                System.out.println("\n人口超过7017230的城市:");
                DatabaseModification.showResults("City", rset1);

                Statement stmt2 = conn.createStatement();
                ResultSet rset2 = stmt2.executeQuery("SELECT * FROM country WHERE country.LifeExpectancy=(SELECT MAX(country.LifeExpectancy)FROM country) ");
                System.out.println("\n国家的平均寿命最长:");
                DatabaseModification.showResults("Country", rset2);

                Statement stmt3 = conn.createStatement();
                ResultSet rset3 = stmt3.executeQuery("SELECT * FROM country WHERE country.LifeExpectancy=(SELECT MIN(country.LifeExpectancy)FROM country) ");
                System.out.println("\n国家的平均寿命最短:");
                DatabaseModification.showResults("Country", rset3);

                Statement stmt4 = conn.createStatement();
                ResultSet rset4 = stmt4.executeQuery("SELECT SUM(Population) FROM country WHERE country.Region=\"Middle East\"");
                System.out.println("\n世界上的所有中东国家的总人口:");
                DatabaseModification.showResults("Country", rset4);

            }
        }
        catch (ClassNotFoundException e)
        {
            System.out.println("SQLException!!!: " + e.getMessage());
            e.printStackTrace();
        }
        catch (Exception e)
        {
            System.out.println("Exception!!!: " + e.getMessage());
            e.printStackTrace();
        }

    }
}