//****************************************************************
//    PP3_5.java        Author: sanjin 
//
//    输入两个点的坐标值（x,y）,然后使用公式计算两点间的距离。
//****************************************************************

import java.util.Scanner;
 
public class PP3_5
{
  //--------------------------------------------------------------
  //  输入两个数值。其中a与b为一个坐标，c与d为一个坐标。
  //--------------------------------------------------------------
  public static void main(String[] args)
  {
    double a, b, c, d, root1, root2, root3, root4, root5, root6;
    Scanner scan = new Scanner(System.in);
   
    System.out.print("Enter the first number: " );
    a = scan.nextDouble();
    System.out.print("Enter the second number: " );
    b = scan.nextDouble();
    System.out.print("Enter the third number: " );
    c = scan.nextDouble();
    System.out.print("Enter the fourth number: " );
    d = scan.nextDouble();
  //-----------------------------------------------------------------
  // 计算两个坐标之间的距离。
  //-----------------------------------------------------------------
    root1 = a - c;
    root2 = b - d;
    root3 = Math.pow(root1, 2);
    root4 = Math.pow(root2, 2);
    root5 = root3 + root4;
    root6 = Math.sqrt(root5);
    System.out.println("Distance: " + root6);
  }
}
