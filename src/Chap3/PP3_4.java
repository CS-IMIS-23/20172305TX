package week3;
//**************************************************************
//  PP3_4.java        Author: sanji
//
//  读入一个Double型的浮点数，然后分别输入不大于该值的最大整数
//  和不小于该值的最小整数。
//**************************************************************

import java.util.Scanner;

public class PP3_4 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double a;
        int b, c;
        System.out.print("Input floating point number :");
        a = scan.nextDouble();
        b = (int) Math.ceil(a);// minimum quantity
        c = (int) Math.floor(a);// greatest measure
        System.out.println("不小于该值的最小数值: " + b);
        System.out.println("不大于该值的最大数值: " + c);
    }
}
