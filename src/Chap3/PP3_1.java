package week3;
//****************************************************************************************
//  PP3_1.java                    Author: sanjin
//
//  用户分别输入名字和姓氏，然后输出一串字符串，该字符串由用户名字的首字母，加上不超过前
//  5个字母的姓氏及一个10-99的随机数组成（假定姓氏至少有5个字母）。
//****************************************************************************************

import java.util.*;

public class PP3_1 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Random generator = new Random();
        String a, b, d, e;
        int num;
        System.out.print("Enter user name: ");//姓名
        a = scan.nextLine();
        System.out.print("Enter user family name: ");//姓氏
        b = scan.nextLine();
        //选取姓氏中的首字母并大写。
        char c = a.charAt(0);
        d = String.valueOf('c');
        e = d.toUpperCase();
        String f = b.substring(0, 5);
        num = generator.nextInt(90) + 10;
        System.out.println(e + f + num);
    }
}
