package week3;
//*************************************************************************
//  PP3_7.java         Author: sanjin
//
//  输入三角形的三条边长，使用Heron公式计算三角形的面积并保存三位小数。
//*************************************************************************

import java.util.Scanner;

public class PP3_7 {
    public static void main(String[] args) {
        double a, b, c, s, S;
        Scanner scan = new Scanner(System.in);
        //  Output three sides of a triangle.
        System.out.print("Enter a side of the triangle :");
        a = scan.nextDouble();
        System.out.print("Enter a side of the triangle :");
        b = scan.nextDouble();
        System.out.print("Enter a side of the triangle :");
        c = scan.nextDouble();
        //  So the Heron formula for the area of the triangle
        s = (a + b + c) / 2;
        S = Math.sqrt(s * (s - a) * (s - c) * (s - b));
        System.out.println("The perimeter of the triangle :" + S);
    }
}
