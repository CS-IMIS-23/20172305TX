//***************************************************************
//  PP3_3.java			Author: sanjin
//
//  生成并输出一个XXX-XXX-XXXX类型的随机电话号码，包括连接线。
//  前三个数字中不能有8或者9,中间三个数字组成的数不能大于655。
//***************************************************************

import java.util.Random;

public class PP3_3
{
  //-------------------------------------------------------------
  //  前三个数字中不能有8或者9。
  //-------------------------------------------------------------
  public static void main(String[] args)
  {
    Random generator = new Random();
    int a, b, c, d, e, f, g, h, i;
    a = generator.nextInt(8);
    b = generator.nextInt(8);
    c = generator.nextInt(8);
  //-------------------------------------------------------------
  //  中间三个数字组成不能大于655。
  //-------------------------------------------------------------
    d = generator.nextInt(556) + 100;  
    e = generator.nextInt(10);
    f = generator.nextInt(10);
    g = generator.nextInt(10);
    h = generator.nextInt(10);
    System.out.println("生成的电话号码:" + a + b + c + "-" + d + "-" + e + f + g + h); 
  } 
}
