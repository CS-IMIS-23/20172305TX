//*****************************************************************************
//  PP3_8.java     Author: sanjin
//
//  产生[20,40]区间内的整型随机数，并显示这些随机数的正弦、余弦和正切值。
//*****************************************************************************

import java.util.Random;

public class PP3_8
{
  //---------------------------------------------------------------------------
  //  产生[20,40]区间内的随机数。
  //---------------------------------------------------------------------------
  public static void main(String[] args)
  {
    Random generator = new Random();
    int num;
    double a, b, c;
    num = generator.nextInt(21) + 20;
    System.out.println("A random integer: " + num);
    
    // public static double cos(double num);
    a = Math.cos(num);
    b = Math.sin(num);
    c = Math.tan(num);
    System.out.println("A random integer's cos: " + a);
    System.out.println("A random integer's sin: " + b);
    System.out.println("A random integer's tan: " + b);
  
  }
}
