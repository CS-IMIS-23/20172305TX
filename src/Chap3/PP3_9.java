package week3;
//**********************************************************************
//  PP3_9.java         Author: sanjin
//
//  编写程序产生[1,20]区间的整型随机数，得到圆柱体的半径和高度，计算
//  圆柱体的体积和表面积。
//**********************************************************************

import java.util.Random;

public class PP3_9 {
    //------------------------------------------------------------------
    //  Randomly generated radius and height.
    //------------------------------------------------------------------
    public static void main(String[] args) {
        int r, h;
        double V, S;
        Random generator = new Random();
        r = generator.nextInt(20) + 1;
        System.out.println("radius: " + r);
        h = generator.nextInt(20) + 1;
        System.out.println("height: " + h);
        V = Math.PI * Math.pow(r, 2) * h; // Calculate the volume.
        S = 2 * Math.PI * r * h; // Calculated surface area.
        System.out.println("The volume of the cylinder: " + V);
        System.out.println("The surface area of a cylinder: " + S);
    }
}
