//*****************************************************************************
//  PP3_2.java    Author: sanjin
//
//  Calculate the cube and sum of two integers.
//*****************************************************************************

import java.awt.*;
import java.util.Scanner;

public class PP3_2 {
    public static void main(String[] args) {
        //---------------------------------------------------------------------------
        //  Enter two integer Numbers.
        //---------------------------------------------------------------------------
        double a, b;
        int c;
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter the number: ");
        a = scan.nextDouble();
        System.out.print("Enter the number: ");
        b = scan.nextDouble();
        //----------------------------------------------------------------------------
        //   Calculate the sum.
        //----------------------------------------------------------------------------
        a = Math.pow(a, 3);
        b = Math.pow(b, 3);
        c = (int) (a + b);
        System.out.println("Two cube of sum: " + c);
    }
}













