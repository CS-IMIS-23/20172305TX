package week3;
//********************************************************************************************
//   PP3_6.java                Author: sanjin
//
//   输入一个球体的半径，然后使用体积和表面积公式，结果保留4位小数。
//********************************************************************************************

import java.util.Scanner;
import java.text.DecimalFormat;

public class PP3_6 {
    public static void main(String[] args) {
        double r, V, S;
        Scanner scan = new Scanner(System.in);
        DecimalFormat fmt = new DecimalFormat("0.0000");
        System.out.print("Input sphere radius: ");
        r = scan.nextDouble();
        V = (4.0 / 3.0) * Math.PI * Math.pow(r, 3);// 体积
        S = 4 * Math.PI * Math.pow(r, 2);// 表面积
        System.out.println("Spheres volume: " + fmt.format(V));
        System.out.println("Sphere surface area: " + fmt.format(S));
    }


}
