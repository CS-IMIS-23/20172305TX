package test.Practice; 

import Practice.Complex;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After; 

/** 
* Complex Tester. 
* 
* @author <Authors name> 
* @since <pre>???? 16, 2018</pre> 
* @version 1.0 
*/ 
public class ComplexTest extends TestCase {
    Complex num1 = new Complex(1,2);
    Complex num2 = new Complex(1,-2);
    Complex num3 = new Complex(2,2);
    Complex num4 = new Complex(2,-2);

    @Test
public void testComplexAdd() {
//TODO: Test goes here...
    assertEquals(new Complex(2,0),num1.ComplexAdd(num2));
} 

/** 
* 
* Method: ComplexSub(Complex a) 
* 
*/ 
@Test
public void testComplexSub() {
//TODO: Test goes here...
    assertEquals(new Complex(0,4),num1.ComplexSub(num2));
}

/**
* 
* Method: ComplexMulti(Complex a) 
* 
*/ 
@Test
public void testComplexMulti() {
//TODO: Test goes here...
    assertEquals(new Complex(8,0),num3.ComplexMulti(num4));
} 

/** 
* 
* Method: ComplexDiv(Complex a) 
* 
*/ 
@Test
public void testComplexDiv() {
//TODO: Test goes here...
    assertEquals(new Complex(0,1),num3.ComplexDiv(num4));
}
} 
