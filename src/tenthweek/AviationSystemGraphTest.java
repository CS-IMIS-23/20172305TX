package tenthweek;

public class AviationSystemGraphTest {
    public static void main(String[] args) {
        AviationSystem<String> cityNetwork = new AviationSystem<>();
        cityNetwork.addVertex("吉林");
        cityNetwork.addVertex("长春");
        cityNetwork.addVertex("四平");
        cityNetwork.addVertex("公主岭");
        cityNetwork.addVertex("松原");
        cityNetwork.addEdge(1,0,112);
        cityNetwork.addEdge(2,0,34);
        cityNetwork.addEdge(2,1,20);
        cityNetwork.addEdge(2,1,17);
        cityNetwork.addEdge(4,3,44);
        cityNetwork.addEdge(4,2,23);
        cityNetwork.addEdge(4,0,5);
        System.out.println("“吉林”到“四平”：");
        System.out.println(cityNetwork.getShorPath("吉林","四平"));
    }
}


