package Chap11;

import java.io.*;
import java.util.Scanner;

public class recursion
{
    public static void main (String[] args) throws IOException
    {
        Scanner scan = new Scanner(System.in);
        System.out.print("请输入一个数字： ");
        int num1 = scan.nextInt();
        int num2 = sum(num1);
        System.out.println("所求的F(n)= " + num2);
        FileWriter file = new FileWriter("D:\\recursion.txt");
        file.write(num1 + "\t\n所求的F(n) = " + num2);
        file.flush();
    }
    static int sum(int num)
    {
        int result = 0;
        if (num == 0)
            return 0;
        else if (num == 1)
            return 1;
        else if (num >= 2)
            result = sum(num - 1) + sum(num - 2);
            return result;
    }
}
