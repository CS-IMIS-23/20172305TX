import secondweek.EmptyCollectionException;

import java.util.Scanner;

public class Pascal {
    public static void main(String[] args) {
        int front = 0, rear = 0, count = 0, line, num1, num2;
        String[] strings = new String[100];
        Scanner scan = new Scanner(System.in);
        System.out.print("请输入杨辉三角行数：");
        line = scan.nextInt();
        System.out.println("杨辉三角（前" + line +"行）：");
        //扩容
        if(count == strings.length){
            String[] larger = new String[strings.length + 1];
            for(int num = 0; num < count; num++){
                larger[num] = strings[front];
                front = (front + 1) % strings.length;
            }
            front = 0;
            rear = count;
            strings = larger;
        }
        strings[rear] = "1";
        rear = (rear + 1) % strings.length;
        count++;
        //仅输入一行的话，就输出1
        if(line == 1){
            System.out.println(1);
        }
        //进行循环，输出每一行
        else {
            for (int i = 1; i < line + 1 ; i++) {
                //扩容
                if(count == strings.length){
                    String[] larger = new String[strings.length + 1];
                    for(int num = 0; num < count; num++){
                        larger[num] = strings[front];
                        front = (front + 1) % strings.length;
                    }
                    front = 0;
                    rear = count;
                    strings = larger;
                }
                strings[rear] = "0";
                rear = (rear + 1) % strings.length;
                count++;
                for (int m = 1; m < count; m++) {
                    //取第一个数
                    num1 = Integer.parseInt(strings[front]);
                    if(front == rear)
                        throw new EmptyCollectionException("queue");

                    strings[rear] = null;
                    front = (front + 1) % strings.length;
                    count--;
                    //取第二个数
                    num2 = Integer.parseInt(strings[front]);
                    //扩容
                    if(count == strings.length){
                        String[] larger = new String[strings.length + 1];
                        for(int num = 0; num < count; num++){
                            larger[num] = strings[front];
                            front = (front + 1) % strings.length;
                        }
                        front = 0;
                        rear = count;
                        strings = larger;
                    }
                    //每一行的两个相邻数之和等于下一行的两数中间的数
                    strings[rear] = String.valueOf(num1 + num2);
                    System.out.print(strings[rear] + " ");
                    rear = (rear + 1) % strings.length;
                    count++;
                }
                System.out.println();
            }
        }
    }
}