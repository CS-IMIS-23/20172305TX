//***************************************************************
//  PP2_11.java    	Author: sanjin
//
//  输入一个代表总钱数的双精度值，然后确定每种纸币和硬币所需的最
//  少数量以达到输入的总钱数。
//***************************************************************
 
import java.util.Scanner;

public class PP2_11
{
  //-------------------------------------------------------------
  // 输入一个总钱数
  //-------------------------------------------------------------
public static void main(String[] args)

 {
  double a;
  int b, c, d, e, f, g, h, i, j, k, l, m, n;
 
  Scanner scan = new Scanner(System.in);
  
  System.out.print("Enter your money: ");
  a = scan.nextDouble();

  b =(int)(a * 100);//化为美分
  c = b / 1000;//计算10美元面值
  d = b % 1000;//扣除10美元后的钱
  e = d / 500;//计算5美元面值
  f = d % 500;//扣除5美元后的钱
  g = f / 100;//计算1美元面值
  h = f % 100;//扣除1美元后的钱
  i = h / 25;//计算25美分面值
  j = h % 25;//扣除25美分后的钱
  k = j / 10;//计算10美分面值
  l = j % 10;//扣除10美分后的钱
  m = l / 5;//计算5美分面值
  n = l % 5;//计算1美分面值
  
  System.out.println( c + " ten dollar bills");
  System.out.println( e + " five dollar bills");
  System.out.println( g + " one dollar bills");
  System.out.println( i + " quarters");
  System.out.println( k + " dimes");
  System.out.println( m + " nickles");
  System.out.println( n + " pennies");
 }
}





