//**********************************************
// PP2_4.java           Author: sanjin
//
// 输出某人的姓名、年龄、毕业学校及其宠物的名字。
// 并显示一段文字。
//**********************************************

import java.util.Scanner;

public class PP2_4
{
  public static void main(String[] args)
  {
    String name;
    String age;
    String college;
    String petname; 
    Scanner scan = new Scanner(System.in);
    
    // type name
    System.out.print("name: ");
    name = scan.nextLine();   
     
    // type age
    System.out.print("age: ");           
    age = scan.nextLine();  
    
    // type college
    System.out.print("college: ");   
    college = scan.nextLine();   
    
    // type petname
    System.out.print("petname: ");   
    petname = scan.nextLine();
           
    System.out.print("Hello,my name is " +  name  + " and I am " + age);
    System.out.print(" years" + "\nold. I'm enjoying my time at " + college);
    System.out.print(", though " + "\nI miss my pet " + petname);
    System.out.println(" very much!");
  }
}

