//*********************************************************
//  PP2_8.java        	Author: sanjin
//
//  以小时、分、秒为时间长度，然后全部换算成秒，并输出结果。
//*********************************************************

import java.util.Scanner;
 
public class PP2_8
{
 public static void main(String[] args)

 {
   int hour, minute, second;
  
   Scanner scan = new Scanner(System.in);

   System.out.print("Enter hours: ");
   //calculate hours
   hour = scan.nextInt();

   System.out.print("Enter minute: ");
   //calculate minutes
   minute = scan.nextInt();
  
   System.out.print("Enter second: ");
   //calculate seconds
   second = scan.nextInt();
  
   second = hour * 3600 + minute * 60 + second;
  
   System.out.println("Total time: " + second);
 }
}

   

