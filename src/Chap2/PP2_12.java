//**************************************************************************
// PP2_12.java                Author: sanjin
//
// 读取正方形的边长，输出该正方形的周长和面积。
//**************************************************************************

import java.util.Scanner;
 
public class PP2_12
{
  //---------------------------------------------------------
  //Calculate the length of the square that the user entered.
  //---------------------------------------------------------
  public static void main(String[] args)
  {
    short a, perimeter, area;
    
    Scanner scan = new Scanner(System.in);
   
    System.out.print("Enter the side length of the square: ");
    a = scan.nextShort();
    
    perimeter = (short)(4 * a);
    area = (short)(a * a);
    
    System.out.println("perimeter of the square: " + perimeter);
    System.out.println("area of the square: " + area);
  }
}
    





