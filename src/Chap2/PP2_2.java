//**************************************************************************
// PP2_2.java         Author: sanjin
//
// 读取三个整数并输出三者的平均值。
//**************************************************************************

import java.util.Scanner;

public class PP2_2
{
  //------------------------------------------------------------------------
  // Calculate the average value of the three values entered by the user.
  //------------------------------------------------------------------------
  public static void main(String[] args)
  {
    int a, b, c;
    double average;
   
    Scanner scan = new Scanner(System.in);
 
    System.out.print("Enter three number: ");
   
    a = scan.nextInt();
    b = scan.nextInt();
    c = scan.nextInt();
   
    average = (a + b + c) / 3.0;
    System.out.println("average: " + average);
  }
}


