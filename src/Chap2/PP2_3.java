//**************************************************************************
// PP2_3.java                    Author: sanjin
//
// 读取两个浮点数，输入两者的和、差及乘积。   
//**************************************************************************
  
import java.util.Scanner;

public class PP2_3
{
  //------------------------------------------------------------------------
  // Calculates the two floating point Numbers entered by the user.
  //------------------------------------------------------------------------
  public static void main(String[] args)
  {
    double a, b, sum, difference, product;
    
    Scanner scan = new Scanner(System.in);
    System.out.print("Enter two number: ");
    a = scan.nextDouble();
    b = scan.nextDouble();
   
    sum = a + b;
    difference = a - b;
    product = a * b;
    System.out.println("Sum: "+ sum);
    System.out.println("Difference: "+ difference);
    System.out.println("Product: "+ product);
  }
}
