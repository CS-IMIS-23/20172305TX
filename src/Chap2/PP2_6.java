//*******************************************************************
//  PP2_6.java                   Author: sanjin
// 
//  以浮点数的形式将英里数转换为浮点数。
//*******************************************************************

import java.util.Scanner;

public class PP2_6
{
  //-----------------------------------------------------------------
  // Read the number of miles the user entered.
  //-----------------------------------------------------------------
  public static void main(String[] args)
  {
    double miles, kilometer; 
    Scanner scan = new Scanner(System.in);
  
    System.out.print("Enter the miles: ");
    miles = scan.nextDouble();

    kilometer = miles * 1.60935;
    
    System.out.println("Corresponding kilometer: " + kilometer);
  }
}
