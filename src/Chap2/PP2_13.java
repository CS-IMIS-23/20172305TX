//**********************************************************************
//  PP2_13.java                  Author: sanjin
//
//  输入两个整数分别作为分数的分子和分母，然后输出其小数表示。
//**********************************************************************
 
import java.util.Scanner;

public class PP2_13
{
  public static void main(String[] args)
  {
    int a, b;
    double c;
    Scanner scan = new Scanner(System.in);  

    System.out.print("Enter molecules: ");// Enter integers as molecules.
    a = scan.nextInt();

    System.out.print("Enter denominator: ");//Enter an integer as the denominator.
    b = scan.nextInt();
 
    c =(double) a / b;
    System.out.println("Fractions: " + c ); 
  }
}
