//**************************************************************************
// PP2_5.java                Author: sanjin
// 
// 读取用户输入的华氏温度值，然后转换成摄氏温度值。
//**************************************************************************
 
import java.util.Scanner;
 
public class PP2_5
{
  //------------------------------------------------------------------------
  //Calculate the user's Celsius value through the input of Fahrenheit.
  //------------------------------------------------------------------------
  public static void main(String[] args)
  {
    final int BASE = 32;
    final double CONVERSION_FACTOR = 5.0 / 9.0;
    double fahrenheitTemp, celsiusTemp;
   
    Scanner scan = new Scanner(System.in);
 
    System.out.print("Enter your fahrenheitTemp: ");
    fahrenheitTemp = scan.nextDouble();
 
    celsiusTemp = CONVERSION_FACTOR * (fahrenheitTemp - BASE);
  
    System.out.println("celsiusTemp: " + celsiusTemp);
  }
}
