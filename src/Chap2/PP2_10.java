//*******************************************************************
//  PP2_10.java              Author: sanjin
//
//  输入总面值的钱数，并以美元、美分的形式输出钱数。
//  读取分别代表25美分、10美分、5美分和1美分的硬币数量的整数。 
//*******************************************************************
 
import java.util.Scanner;

public class PP2_10
{
  public static void main(String[] args)
  {
   //------------------------------------------------------------------
   // Read the money and show it in dollars and cents.
   //------------------------------------------------------------------
   int a, b, c, d, e;
   double f;
   Scanner scan = new Scanner(System.in);
 
   System.out.print("Enter your Quarters: ");
   a = scan.nextInt();
  
   System.out.print("Enter your Dimes: ");
   b = scan.nextInt();
  
   System.out.print("Enter your nickles: ");
   c = scan.nextInt();
  
   System.out.print("Enter your pennies: ");
   d = scan.nextInt();
 
   //------------------------------------------------------------------
   // 进行计算美分美元为单位的总面值，美元：美分=1：100.
   //------------------------------------------------------------------
 
   e = a * 25 + b * 10 + c * 5 + d;
   f = (double) e / 100;
   System.out.println("Dollars: " + f );
   System.out.println("Cents: " + e );
  }
}
