//************************************************************
//  PP2_7.java             Author: sanjin
//
//  读入表示速度和旅行距离的整形值，然后以浮点型显示旅行所需的
//  时间。
//************************************************************

import java.util.Scanner;

public class PP2_7
{
  //----------------------------------------------------------
  // Read speed and distance.
  //----------------------------------------------------------
  public static void main(String[] args)
  {
    int v, s;
    double t;
    Scanner scan = new Scanner(System.in);
   
    System.out.print("Enter your speed: ");
    v = scan.nextInt();
    
    System.out.print("Enter your distance: ");
    s = scan.nextInt();
   
    t =(double) s / v ;
    System.out.println("time: " + t );
  }
}
    
