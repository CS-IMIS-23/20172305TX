//***********************************************************************
//  PP2_9.java      	Author: sanjin
//
//  反向运行程序PP2_8.java，读入一个以秒为单位的时间长度，然后换算成小时、
//  分和秒的组合方式并输出结果。
//***********************************************************************

import java.util.Scanner;

public class PP2_9
{
  //--------------------------------------------------------------------
  // The input time is decomposed into hours, minutes, and seconds.
  //--------------------------------------------------------------------
  public static void main(String[] args)
  {
    int time, hour, minute, second, a;
    Scanner scan = new Scanner(System.in);

    System.out.print("Enter time: ");
    time = scan.nextInt();
 
    hour = time / 3600;
    a = time % 3600;
    minute = a / 60;
    second = a % 60;
    
    System.out.println("hour: " + hour);
    System.out.println("minute: " + minute);
    System.out.println("second: " + second);
   }
}
      










