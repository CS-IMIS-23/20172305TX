import java.io.*;
import java.net.Socket;

public class SocketClient {
    public static void main(String[] args) throws IOException {
        //1.建立客户端Socket连接，指定服务器位置和端口
        // Socket socket = new Socket("localhost",8080);
        Socket socket = new Socket("192.168.70.128",8800);

        //2.得到socket读写流
        OutputStream outputStream = socket.getOutputStream();
        //  PrintWriter printWriter = new PrintWriter(outputStream);
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
        //输入流
        InputStream inputStream = socket.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"UTF-8"));
        //3.利用流按照一定的操作，对socket进行读写操作
        String info = "12 15 8 100 25 34 19 ";
//        String info = new String(info1.getBytes("utf-8"));
        //     printWriter.write(info);
        //     printWriter.flush();
        outputStreamWriter.write(info);
        outputStreamWriter.flush();
        socket.shutdownOutput();
        //接收服务器的响应
        System.out.println("传输信息为：" + info);
        String reply = null;
        while (!((reply = bufferedReader.readLine()) ==null)){
            System.out.println("接收来自虚拟机服务器的信息为：" + reply);
        }

        //4.关闭资源
        bufferedReader.close();
        inputStream.close();
        outputStreamWriter.close();
        //printWriter.close();
        outputStream.close();
        socket.close();
    }
}
