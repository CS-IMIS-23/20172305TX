package Chap10;
//-------------------------------------------------------------------------------------
//  Payable.java         Author: sanjin
//
//  Finish the homework of the Chap10(10.1)
//-------------------------------------------------------------------------------------
public interface Payable
{
    public void payday();
}
