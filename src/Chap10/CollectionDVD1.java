package Chap10;
import java.text.NumberFormat;
import java.util.Collection;
public class CollectionDVD1 implements Comparable<CollectionDVD1>{
        private DVD1 [] collection;
        private int count;
        private double totalCost;

        private String title, director;
        private int year;
        private double cost;
        private boolean bluray;
        //------------------------------------------------------------------------------------
        //  Constructor: Creates an intially empty collection.
        //------------------------------------------------------------------------------------
        public CollectionDVD1(String title, String director, int year, double cost, boolean bluray) {
            collection = new DVD1[100];
            count = 0;
            totalCost = 0.0;

            this.title = title ;
            this.director = director ;
            this.year = year;
            this.cost = cost;
            this.bluray = bluray ;

        }

        //--------------------------------------------------------------------------------------------
        //  Adds a DVD to the collection, increasing the size of the collection array if neccessary.
        //--------------------------------------------------------------------------------------------
        public void addDVD(String title, String director, int year, double cost, boolean bluray) {
            if (count == collection.length)
                increaseSize();

            collection[count] = new DVD1(title, director, year, cost, bluray);
            totalCost += cost;
            count++;
        }

        //----------------------------------------------------------------------------------------------------
        //  Returns a report describing the DVD collection.
        //----------------------------------------------------------------------------------------------------
        public String toString() {
            String report = "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
                report += "DVD Title: " + title + "\nDirector: " + "\nYear: " + year + "\nCost: " + cost + "\nBlurary:" + bluray + "\n";
            return report;


        }

        //------------------------------------------------------------------------------------------------------------------
        //  Increase the capacity of the collection by creating a larger array and copying the existing collection into it.
        //------------------------------------------------------------------------------------------------------------------
        private void increaseSize() {
            DVD1[] temp = new DVD1[collection.length * 2];

            for (int dvd = 0; dvd < collection.length; dvd++)
                temp[dvd] = collection[dvd];

            collection = temp;
        }
        //-----------------------------------------------------------------------------
        //  Rank by name
        //-----------------------------------------------------------------------------
        public int compareTo(CollectionDVD1 name)
        {
            int result;

            String nameTitle = name.getTitle();

            result = title.compareTo(nameTitle);
            return result;
        }
        public String getTitle ()
        {
            return title;
        }
}

