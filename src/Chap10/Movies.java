package Chap10;
//-------------------------------------------------------------------------------------
//  Movies.java         Author: sanjin
//
//  Finish the homework of the Chap10(10.5)
//-------------------------------------------------------------------------------------
public class Movies
{
        public static void main(String[] args)
        {
            CollectionDVD1[] movies = new CollectionDVD1[7];

            movies[0]=new CollectionDVD1("The Godfather", "Francis Ford Coppola", 1972, 24.95, true) ;
            movies[1]=new CollectionDVD1("District 9", "Neill Blomkamp", 2009, 19.95, false);
            movies[2]=new CollectionDVD1("Iron Man", "Jon Favreau", 2008, 15.95, false);
            movies[3]=new CollectionDVD1("All About Eve", "Joseph Mankiewicz", 1950, 17.50, false);
            movies[4]=new CollectionDVD1("The Matrix", "Andy & Lana Wachowski", 1999, 19.95, true);
            movies[5]=new CollectionDVD1("Iron Man 2", "Jon Favreau", 2010, 22.99, false);
            movies[6]=new CollectionDVD1("Casablanca", "Michael Curtiz", 1942, 19.95, false);

            Sorting .selectionSort1(movies);

            for(CollectionDVD1 movie: movies)
            System .out .println(movie);
        }
}



