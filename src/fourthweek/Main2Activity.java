package com.example.administrator.productlinked;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        final ProductLinked productLinked = new ProductLinked();

        Button button1 = (Button)findViewById(R.id.button1);
        Button button2 = (Button)findViewById(R.id.button2);
        Button button3 = (Button)findViewById(R.id.button3);
        Button button4 = (Button)findViewById(R.id.button4);
        Button button5 = (Button)findViewById(R.id.button5);
        Button button6 = (Button)findViewById(R.id.button6);
        Button button7 = (Button)findViewById(R.id.button7);
        Button button8 = (Button)findViewById(R.id.button8);
        Button button9 = (Button)findViewById(R.id.button9);
        Button button10 = (Button)findViewById(R.id.button10);

        final EditText name = (EditText)findViewById(R.id.editText1);
        final EditText price = (EditText)findViewById(R.id.editText2);
        final EditText kind = (EditText)findViewById(R.id.editText3);
        final EditText barCode = (EditText)findViewById(R.id.editText4);
        final EditText xianshi = (EditText)findViewById(R.id.editText5);
        //addToFront
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productLinked.addToFront(new Product(name.getText().toString(), Integer.parseInt(price.getText().toString()), kind.getText().toString(), barCode.getText().toString()));
                xianshi.setText(productLinked.toString(), TextView.BufferType.EDITABLE);

                name.setText("");
                price.setText("");
                kind.setText("");
                barCode.setText("");
            }
        });
        //addToRear
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productLinked.addToRear(new Product(name.getText().toString(), Integer.parseInt(price.getText().toString()), kind.getText().toString(), barCode.getText().toString()));
                xianshi.setText(productLinked.toString(), TextView.BufferType.EDITABLE);
                name.setText("");
                price.setText("");
                kind.setText("");
                barCode.setText("");
            }
        });
        //removeFirst
        button3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(0 == productLinked.size()){
                    Toast toast = Toast.makeText(Main2Activity.this,"商品链内已无商品！！！",Toast.LENGTH_LONG);
                    toast.show();
                }
                else{
                    productLinked.removeFirst();
                    xianshi.setText(productLinked.toString(),TextView.BufferType.EDITABLE);
                }
            }
        });
        //removeLast
        button4.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(0 == productLinked.size()){
                    Toast toast = Toast.makeText(Main2Activity.this,"商品链内已无商品！！！",Toast.LENGTH_LONG);
                    toast.show();
                }
                else{
                    productLinked.removeLast();
                    xianshi.setText(productLinked.toString(),TextView.BufferType.EDITABLE);
                }
            }
        });
        //remove
        button5.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(0 == productLinked.size()){
                    Toast toast = Toast.makeText(Main2Activity.this,"商品链内已无商品！！！",Toast.LENGTH_LONG);
                    toast.show();
                    name.setText("");
                    price.setText("");
                    kind.setText("");
                    barCode.setText("");
                }
                else{
                    productLinked.remove(new Product(name.getText().toString(), Integer.parseInt(price.getText().toString()), kind.getText().toString(), barCode.getText().toString()));
                    xianshi.setText(productLinked.toString(), TextView.BufferType.EDITABLE);
                    name.setText("");
                    price.setText("");
                    kind.setText("");
                    barCode.setText("");
                }
            }
        });
        //first
        button6.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                xianshi.setText(productLinked.first().toString(),TextView.BufferType.EDITABLE);
            }
        });
        //last
        button7.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                xianshi.setText(productLinked.last().toString(),TextView.BufferType.EDITABLE);
            }
        });
        //contains
        button8.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(productLinked.contains(new Product(name.getText().toString(), Integer.parseInt(price.getText().toString()), kind.getText().toString(), barCode.getText().toString())))
                    xianshi.setText("True!!",TextView.BufferType.EDITABLE);
                else
                    xianshi.setText("False!!",TextView.BufferType.EDITABLE);
            }
        });
        //find
        button9.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                String result = productLinked.find(barCode.getText().toString());
                xianshi.setText(result.toString(),TextView.BufferType.EDITABLE);
            }
        });
        //sorting
        button10.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                productLinked.sorting();
                xianshi.setText(productLinked.toString(),TextView.BufferType.EDITABLE);
            }
        });
    }
}
