package fourthweek;

import secondweek.EmptyCollectionException;
import secondweek.LinearNode;

public class ProductLinked {
    private int count;
    private LinearNode<Product> head, tail;
    public ProductLinked(){
        count = 0;
        head = tail = null;
    }
    public void addToFront(Product element) {
        LinearNode<Product> node = new LinearNode<Product>(element);
        LinearNode<Product> temp = head;
        if(isEmpty())
            head = tail = node;
        else {
            node.setNext(head);
            while(temp.getNext() != null){
                temp = temp.getNext();
            }
            tail = temp;
            head = node;
        }
        count++;
    }
    public void addToRear(Product element){
        LinearNode<Product> node = new LinearNode<Product>(element);
        if(isEmpty())
            head = node;
        else
            tail.setNext(node);
        tail = node;
        count++;
    }
    public void addAfter(Product element, Product target) {
        LinearNode current = head;
        LinearNode node = new LinearNode(element);

        if (isEmpty())
        {
            head = node;
        }

        while ((current!= null) && (current.getElement().equals(target)))
        {
            current = current.getNext();
        }
        if(current.getNext() == null){
            current.setNext(node);
            tail = node;
        }else{
            node.setNext(current.getNext());
            current.setNext(node);
        }

        count++;
    }
    public Product removeFirst() {
        if (isEmpty())
            throw new EmptyCollectionException("LinkedList");

        LinearNode<Product> current = head;

        if (size() == 1)
            head = tail = null;
        else
            head = current.getNext();

        count--;
        return current.getElement();
    }
    public Product removeLast() {
        if (isEmpty())
            throw new EmptyCollectionException("LinkedList");
        boolean found = false;
        LinearNode<Product> previous = null;
        LinearNode<Product> current = head;
        while (current != null && !found) {
            if (current.equals(tail)) {
                found = true;
            } else {
                previous = current;
                current = current.getNext();
            }
        }
        if (size() == 1)
            head = tail = null;
        else {
            tail = previous;
            tail.setNext(null);
        }
        count--;
        return current.getElement();
    }
    public Product remove(Product targetElement) {
        if (isEmpty())
            throw new EmptyCollectionException("LinkedList");

        boolean found = false;
        LinearNode<Product> previous = null;
        LinearNode<Product> current = head;

        while (current != null && !found)
            if (targetElement.equals(current.getElement()))
                found = true;
            else {
                previous = current;
                current = current.getNext();
            }
        if (!found)
            throw new ElementNotFoundException("LinkedList");

        if (size() == 1)
            head = tail = null;
        else if (current.equals(head))
            head = current.getNext();
        else if (current.equals(tail))
        {
            tail = previous;
            tail.setNext(null);
        } else
            previous.setNext(current.getNext());

        count--;
        return current.getElement();
    }
    public String find(String name){
        if(isEmpty()){
            System.out.println("商品链中没有商品！！！");
        }
        LinearNode<Product> temp = head;
        while(temp != null && !temp.getElement().getName().equals(name)){
            temp = temp .getNext();
        }
        return temp.getElement().toString();
    }
    public Product first() {
        if (isEmpty())
            throw new EmptyCollectionException("queue");
        else
            return head.getElement();
    }
    public Product last() {
        if (isEmpty())
            throw new EmptyCollectionException("queue");
        else
            return tail.getElement();
    }
    public boolean contains(Product target) {
        LinearNode current = head;
        while (current != null && !target.equals(current.getElement())) {
            current = current.getNext();
        }

        return current == null;
    }
    public boolean isEmpty() {
        return count == 0;
    }
    public int size() {
        return count;
    }
    @Override
    public String toString() {
        LinearNode<Product> temp = head;
        String result = "";
        while(temp != null)
        {
            result += temp.getElement().toString() + "\n";
            temp = temp.getNext();
        }
        return result;
    }
    public void sorting() {
        Product product;
        LinearNode<Product> temp1 ,temp2 = head;
        while (temp2 != null) {
            temp1 = temp2;
            LinearNode<Product> current = temp1.getNext();
            while (current != null) {
                if (current.getElement().compareTo(temp1.getElement()) < 0) {
                    temp1 = current;
                }
                current = current.getNext();
            }
            product = temp1.getElement();
            temp1.setElement(temp2.getElement());
            temp2.setElement(product);
            temp2 = temp2.getNext();
        }
    }
}