package fourthweek;

import secondweek.EmptyCollectionException;
import secondweek.LinearNode;

public class ProgramOfStudys {
    private int count;
    private LinearNode<Course> head, tail;

    public ProgramOfStudys() {
        count = 0;
        head = tail = null;
    }

    public void addCourse(Course course) {
        LinearNode<Course> node = new LinearNode(course);
        LinearNode<Course> temp1 = null,temp2 = head;

        while((temp2.getElement().compareTo(course) < 0) && (temp2 != null)){
            temp1 = temp2;
            temp2 = temp2.getNext();
        }

        if(temp1 == null){
            head = tail =  node;
        }
        else{
            temp1.setNext(node);
        }

        node.setNext(temp2);

        if(node.getNext() == null)
            tail = node;

        count++;
    }

    public Course removeFirst() {
        if (isEmpty())
            throw new EmptyCollectionException("LinkedList");

        LinearNode<Course> current = head;
        if (size() == 1)
            head = tail = null;
        else
            head = current.getNext();

        count--;
        return current.getElement();
    }
    public Course removeLast() {
        if (isEmpty())
            throw new EmptyCollectionException("LinkedList");
        boolean found = false;

        LinearNode<Course> previous = null;
        LinearNode<Course> current = head;

        while (current != null && !found) {
            if (current.equals(tail)) {
                found = true;
            } else {
                previous = current;
                current = current.getNext();
            }
        }

        if (size() == 1)
            head = tail = null;
        else {
            tail = previous;
            tail.setNext(null);
        }
        count--;
        return current.getElement();
    }

    public Course remove(Course targetElement) {
        if (isEmpty())
            throw new EmptyCollectionException("LinkedList");

        boolean found = false;
        LinearNode<Course> previous = null;
        LinearNode<Course> current = head;

        while (current != null && !found)
            if (targetElement.equals(current.getElement()))
                found = true;
            else {
                previous = current;
                current = current.getNext();
            }

        // 如果没有发现需要删除的结点
        if (!found)
            throw new ElementNotFoundException("LinkedList");

        if (size() == 1) // only one element in the list
            head = tail = null;
        else if (current.equals(head)) // target is at the head
            head = current.getNext();
        else if (current.equals(tail)) // target is at the tail
        {
            tail = previous;
            tail.setNext(null);
        } else
            // target is in the middle
            previous.setNext(current.getNext());

        count--;
        return current.getElement();
    }

    public Course first() {
        if (isEmpty())
            throw new EmptyCollectionException("queue");
        else
            return head.getElement();
    }
    public Course last() {
        if (isEmpty())
            throw new EmptyCollectionException("queue");
        else
            return tail.getElement();
    }

    public boolean contains(Course target) {
        LinearNode temp = head;
        while((temp.getElement() != target)&&(temp !=  null)){
            temp = temp.getNext();
        }
        return temp.getElement() == target;
    }

    public boolean isEmpty() {
        return count == 0;
    }

    public int size() {
        return count;
    }

    @Override
    public String toString()
    {
        LinearNode<Course> temp = head;
        String result = "";
        while(temp != null)
        {
            result += temp.getElement().toString() + "\n";
            temp = temp.getNext();
        }
        return result;
    }
}

