package fourthweek.Linked;

public class LinkedListTest {
    public static void main(String args[]){
        LinkedUnorderedList linkedUnorderedList = new LinkedUnorderedList();
        linkedUnorderedList.addToRear("1.20172305谭鑫");
        linkedUnorderedList.addToRear("2.网络空间安全系");
        linkedUnorderedList.addToRear("3.2系3班");
        linkedUnorderedList.addToRear("4.吉林省");
        System.out.println(linkedUnorderedList.toString());
        System.out.println("列表头部：" + linkedUnorderedList.first());
        System.out.println("列表尾部：" + linkedUnorderedList.last());
        System.out.println("列表是否为空：" + linkedUnorderedList.isEmpty());
        System.out.println("列表的元素数目：" + linkedUnorderedList.size());
        System.out.println("列表删除头部元素：" + linkedUnorderedList.removeFirst() + "\t" + linkedUnorderedList.first());
        System.out.println("列表删除尾部元素：" + linkedUnorderedList.removeLast() + "\t" + linkedUnorderedList.last());
        System.out.println("列表内是否含有元素'2.网络空间安全系'：" + linkedUnorderedList.contains("2.网络空间安全系"));
        System.out.println("列表内删除元素'2.网络空间安全系'：" + linkedUnorderedList.remove("2.网络空间安全系"));
        System.out.println(linkedUnorderedList.toString());
    }
}
