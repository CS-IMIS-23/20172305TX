package fourthweek;

public class POSTesters {
    public static void main(String args[]){
        ProgramOfStudys pos = new ProgramOfStudys();
        pos.addCourse(new Course("CS", 101, "Introduction to Programming", "A-"));
        pos.addCourse(new Course("ARCH", 305, "Building Analysis", "A"));
        pos.addCourse(new Course("GER", 210, "Intermediate German"));
        pos.addCourse(new Course("CS", 320, "Computer Architecture"));
        pos.addCourse(new Course("THE", 201, "The Theatre Experience"));

        System.out.println(pos);
    }
}
