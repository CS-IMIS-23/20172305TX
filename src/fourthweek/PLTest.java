package fourthweek;

public class PLTest {
    public static void main(String args[]){
        ProductLinked productLinked = new ProductLinked();
        productLinked.addToFront(new Product("海飞丝去屑洗化",23,"洗护产品", "6_954_767_470_573"));
        productLinked.addToRear(new Product("八寸护边碗套",21,"五金","3_234_457_409_203"));
        productLinked.addToRear(new Product("乐事薯片（原味)", 3, "膨化食品","8_564_967_098_276"));
        productLinked.addToRear(new Product("乐事薯片（番茄味)", 3, "膨化食品","8_333_765_008_206"));
        productLinked.addToRear(new Product("娃哈哈", 2, "饮品","8_213_477_007_488"));
        productLinked.addToRear(new Product("旺仔牛奶", 5, "饮品","3_766_254_323_117"));
        productLinked.addAfter(new Product("百事可乐", 3, "饮品","2_064_004_094_001"),new Product("乐事薯片（原味)", 5, "膨化食品","8_564_954_098_276"));
        System.out.println("商品链中是否有‘八寸护边碗套’" + productLinked.find("八寸护边碗套"));
        System.out.println("商品链中手否含有‘海飞丝去屑洗化,23,洗护产品, 6_954_767_470_573’" + productLinked.contains(new Product("海飞丝去屑洗化",23,"洗护产品", "6_954_767_470_573")));
        System.out.println("排序前：\n" + productLinked.toString());
        productLinked.sorting();
        System.out.println("排序后：\n" + productLinked.toString());
        System.out.println("商品链的头部：" + productLinked.first());
        System.out.println("商品链的尾部：" + productLinked.last());
        System.out.println("商品链是否为空：" + productLinked.isEmpty());
        System.out.println("商品数目：" + productLinked.size());
        System.out.println("商品删除头部：" + productLinked.removeFirst() + "\t商品链的头部：" + productLinked.first());
        System.out.println("商品删除尾部：" + productLinked.removeLast() + "\t商品链的尾部：" + productLinked.last());
        productLinked.addToFront(new Product("旺仔牛奶", 2, "饮品","3_766_254_323_117"));
        System.out.println("排序前：\n" + productLinked.toString());
        productLinked.sorting();
        System.out.println("排序后：\n" + productLinked.toString());
        System.out.println("商品链内是否含有商品'乐事薯片（原味)\", 3, \"膨化食品\",\"8_564_967_098_276\"'：" + productLinked.contains(new Product("乐事薯片（原味)", 3, "膨化食品","8_564_967_098_276")));
        System.out.println(productLinked.toString());
    }
}
