package fourthweek;

public class Product implements Comparable<Product>{

    private String name;
    private int price;
    private String kind;
    private String barCode;
    private String introduce;
    public Product(String name, int price, String kind, String barCode, String introduce) {
        this.name = name;
        this.price = price;
        this.kind = kind;
        this.barCode = barCode;
        this.introduce = introduce;
    }
    public Product(String name, int price, String kind, String barCode)
    {
        this(name, price, kind, barCode, "");
    }
    public String getName() {
        return name;
    }
    public int getPrice() {
        return price;
    }
    public String getKind() {
        return kind;
    }
    public String getBarCode() {
        return barCode;
    }
    public String getIntroduce() {
        return introduce;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setPrice(int price) {
        this.price = price;
    }
    public void setKind(String kind) {
        this.kind = kind;
    }
    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }
    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    @Override
    public String toString() {
        if(introduce == "")
            return "商品名称：" + name + "\t价格：" + price + "\t种类：" + kind + "\t条形码：" + barCode + "\t商品介绍：无";
        else
            return " 商品名称：" + name + "\t价格：" + price + "\t种类：" + kind + "\t条形码：" + barCode + "\t商品介绍：" + introduce;
    }
    @Override
    public int compareTo(Product product) {
        if(name == product.getName()){
            return String.valueOf(this.price).compareTo(String.valueOf(product.price));
        }
        else {
            return this.name.compareTo(product.getName());
        }
    }
}

