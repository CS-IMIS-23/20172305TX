//*************************************************************************************
//  factorial.java  		Author: sanjin
//
//  Test factoral.
//*************************************************************************************

import java.util.Scanner;

public class factorial
{
 public static void main(String[] args)
 {
   Scanner scan = new Scanner(System.in);
   //for-loop
   System.out.println("Test for-loop.");
   int sum1 = 1;
   System.out.print("Enter a number: ");
   int num1 = scan.nextInt();
   
   if (num1 < 0)
     System.out.println("Please enter a positive integer.");
   else
     if (num1 == 0)
       System.out.println("number's factorial: " + 1);
     else
      { for (int num = 1; num <= num1; num++)
        sum1 *= num;
   
        System.out.println("number's factorial: " + sum1);
      }

   //while-loop
   System.out.println("Test while-loop.");
   int sum2 = 1;
   System.out.print("Enter a number: ");
   int num3 = scan.nextInt();
   int num2 = 1;
   
   if (num3 < 0)
     System.out.println("Please enter a positive integer.");
   else
     if (num3 == 0)
       System.out.println("number's factorial: " + 1);
     { while (num2 <= num3) 
       {  sum2 *= num2;   
           num2++;}
       System.out.println("number's factorial: " + sum2);
     }
 }
}
