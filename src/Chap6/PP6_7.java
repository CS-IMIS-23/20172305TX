//*************************************************************************************
//    PP6_7.java                Author: sanjin
//
//    Output pattern adout P191 a, b, c, d.
//*************************************************************************************
public class PP6_7
{
  //-----------------------------------------------------------------------------------
  // Prints a triangle shape asterisk (star) characters.
  //-----------------------------------------------------------------------------------
  public static void main(String[] args)
  {
   System.out.println("a图案如下:");
   for (int row = 10; row >= 1; row--)
    {
      for (int star = 1; star <= row; star++)
        System.out.print("*");
        System.out.println();
      }
    
    System.out.println();
    System.out.println("b图案如下:");
    for (int row = 1; row <= 10; row++)
    {
      for (int black = 1; black <= 10 - row; black++)
      { 
        System.out.print(" ");
      }
      for (int star = 1; star <= row; star++)
      {
        System.out.print("*");}
        System.out.println();
      }
   
    System.out.println();
    System.out.println("c图案如下:");
    for (int row = 10; row >= 1; row--)
    {
      for (int black = 1; black <= 10 - row; black++)
      { 
        System.out.print(" ");
      }
      for (int star = 1; star <= row; star++)
      { 
        System.out.print("*");}
        System.out.println(" ");  
      }
  
    System.out.println();
    System.out.println("d图案如下:");
    for (int row = 5; row >=0; row--)
    {
      for (int black =1 ; black <= row; black++)
      {
        System.out.print(" ");
      }
      for (int star = 1; star <= 9 - 2 * row ; star++)     
      {  
        System.out.print("*");
      }
        System.out.println();}
    
    for (int row = 1; row <= 5; row++)
    {
      for (int black = 1; black < row; black++)
      {
        System.out.print(" ");
      }
      for (int star = 1; star <= 11 - 2 * row; star++)
      {
        System.out.print("*");
      }
        System.out.println();}
  }
}   
