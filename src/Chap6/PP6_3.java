//*************************************************************************************
//  PP6_3.java			Author:sanjin
//
//  12 * 12 multiplication table
//*************************************************************************************
public class PP6_3
{
  public static void main(String[] args)
  {
    int num1, num2;
    System.out.println("12 * 12 multiplication table: ");
    for (num1 = 1; num1 <= 12; num1++)
    {
      for (num2 = 1; num2 <= num1; num2++)
      {
          System.out.print(num1 + " * " + num2 + " = " + (num1 * num2) + "\t");}
      System.out.print("\n");}
  }
}  
