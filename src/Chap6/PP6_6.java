//*************************************************************************************
//  PP6_6.java 		   Author: sanjin
//
//  The number of coins thrown 100 times and the number of times per face.
//*************************************************************************************
public class PP6_6
{
  public static void main(String[] args)
  {
    final int MAX = 100;
    int count = 0;
    Coin myCoin = new Coin();
    for (int flip = 1; flip <= MAX; flip++)
     {  myCoin.flip();
        if (myCoin.isHeads())
        count++;}
    System.out.println(count);
  }
}   
