package Practice;

public class Complex
{
    // 定义属性并生成getter,setter
    static double RealPart;
    static double ImagePart;
    // 定义构造函数
    public Complex(double R,double I)
    {
        RealPart = R;
        ImagePart = I;
    }
    public static double getRealPart()
    {
        return RealPart;
    }
    public static double getImagePart()
    {
        return ImagePart;
    }
    //Override Object
    public boolean equals(Object obj)
    {
        Complex op2 = (Complex) obj;
        Complex temp = this.ComplexSub(op2);
        if(temp.getImagePart() == ImagePart )
        {
            if (temp.getRealPart() == RealPart)
                return true;
            else
                return false;
        }
        else
            return false;
    }
    public String toString()
    {
        String result;
        if(ImagePart >= 0)
            if (ImagePart > 0)
                result = RealPart + "+" + ImagePart + "i";
            else
                result = RealPart + "";
        else
            result = RealPart + "" + ImagePart + "i";
        return result;
    }

    // 定义公有方法:加减乘除
    Complex ComplexAdd(Complex a)
    {
        double b = a.getRealPart() + RealPart;
        double c = a.getImagePart() + ImagePart;
        return new Complex(b, c);
    }
    Complex ComplexSub(Complex a)
    {
        double b = a.RealPart - RealPart;
        double c = a.ImagePart - ImagePart;
        return new Complex(b, c);
    }
    Complex ComplexMulti(Complex a)
    {
        double b = a.getRealPart() * RealPart - a.getImagePart() * ImagePart;
        double c = a.getRealPart() * ImagePart + a.getImagePart() * RealPart;
        return new Complex(b, c);
    }
    Complex ComplexDiv(Complex a)
    {
        double b = (a.getRealPart() * RealPart + a.getImagePart() * ImagePart) / (Math.pow(a.getImagePart(), 2) + Math.pow(a.getRealPart(), 2));
        double c = (a.getRealPart() * ImagePart - a.getImagePart() * RealPart)/ (Math.pow(a.getImagePart(), 2) + Math.pow(a.getRealPart(), 2));
        return new Complex(b, c);
    }
}
