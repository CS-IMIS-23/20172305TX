package Practice;

import junit.framework.TestCase;
import org.junit.Test;

import java.util.concurrent.CopyOnWriteArrayList;

public class ComplexTest extends TestCase
{
    Complex num1 = new Complex(1,2);
    Complex num2 = new Complex(1,-2);
    Complex num3 = new Complex(2,2);
    Complex num4 = new Complex(2,-2);
    Complex num5 = new Complex(2,2);
    @Test
    public void testComplexAdd()
    {
        assertEquals(new Complex(2,0),num1.ComplexAdd(num2));
    }
    @Test
    public void testComplexSub()
    {
        assertEquals(new Complex(0,4),num1.ComplexSub(num2));
    }
    @Test
    public void testComplexMulti()
    {
        assertEquals(new Complex(8,0),num3.ComplexMulti(num4));
    }
    @Test
    public void testComplesDiv()
    {
        assertEquals(new Complex(0,1),num3.ComplexDiv(num4));
    }
    @Test
    public void testequals()
    {
        assertEquals(true,num3.equals(num5));
    }
}