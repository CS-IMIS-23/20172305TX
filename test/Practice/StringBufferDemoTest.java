package Practice;

import junit.framework.TestCase;
import org.junit.Test;

public class StringBufferDemoTest extends TestCase {
    StringBuffer A =  new StringBuffer("StringBuffer");//测试十二个字符
    StringBuffer B = new StringBuffer("StringBufferStringBuffer");//测试二十四个字符
    StringBuffer C = new StringBuffer("StringBufferStringBufferStringBuffer");//测试三十六个字符
    @Test
    public void testcharAt(){
        assertEquals('B',B.charAt(6));
        assertEquals('t',A.charAt(1));
        assertEquals('u',C.charAt(7));
    }
    @Test
    public void testcapacity(){
        assertEquals(28,A.capacity());
        assertEquals(40,B.capacity());
        assertEquals(52,C.capacity());
    }
    @Test
    public void testindexOf(){
        assertEquals(6,A.indexOf("Buf"));
        assertEquals(7,C.indexOf("u"));
        assertEquals(5,C.indexOf("gBu"));

    }
    @Test
    public void testlength(){
        assertEquals(12,A.length());
        assertEquals(24,B.length());
        assertEquals(36,C.length());
    }
}
