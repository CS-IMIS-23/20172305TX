package cn.edu.besti.cs1723.TX2305;

import junit.framework.TestCase;
import org.junit.Test;

public class SearchingTest extends TestCase {
    Integer[] ints1 = {1,2,3,4,5,6,7,8};
    Integer[] ints2 = {8,7,6,5,4,3,2,1};
    Integer[] ints3 = {1,2,3,4,5,6,7,8};
    Integer[] ints4 = {1,2,3,4,5,6,7,8};
    String[] ints5 = {"谭鑫","20172305", "网络空间安全系", "信息管理与信息系统"};
    String[] ints6 = {"谭鑫","YC404", "网络空间安全系", "信息管理与信息系统"};
    Character[] ints7 = {'a','d','8',',','T',';'};
    Character[] ints8 = {'m','f',';','8',',','X'};
    Double[] ints9 = {2.3,5.0,4.87,1.22,8.645};
    Double[] ints10 = {2.7,5.7,1.22,0.4,0.22};

    @Test
    public void testSeraching1()
    {
        assertEquals(true, Searching.linearSearch(ints1,0, ints1.length,8));//正常情况
    }
    public void testSeraching2()
    {
        assertEquals(false, Searching.linearSearch(ints2,0, ints2.length,10));//异常情况
    }
    public void testSeraching3()
    {
        assertEquals(false, Searching.linearSearch(ints3,1, ints3.length,1));//边界情况
    }
    public void testSeraching4()
    {
        assertEquals(true, Searching.linearSearch(ints4,0, ints4.length + 1,2));//边界情况
    }
    public void testSeraching5()
    {
        assertEquals(false, Searching.linearSearch(ints5,0, ints5.length - 1,"tanxin"));//边界情况
    }
    public void testSeraching6()
    {
        assertEquals(false, Searching.linearSearch(ints6,0, ints6.length,"谭鑫"));//异常情况
    }
    public void testSeraching7()
    {
        assertEquals(true, Searching.linearSearch(ints7,0, ints7.length,'X'));//异常情况
    }
    public void testSeraching8()
    {
        assertEquals(true, Searching.linearSearch(ints8,0, ints8.length,'\''));//异常情况
    }
    public void testSeraching9()
    {
        assertEquals(true, Searching.linearSearch(ints9,0, ints9.length,0.22));//异常情况
    }
    public void testSeraching10()
    {
        assertEquals(false, Searching.linearSearch(ints10,0, ints10.length,0.22));//正常情况
    }
}

