package cn.edu.besti.cs1723.TX2305;

import junit.framework.TestCase;
import org.junit.Test;

public class SortingTest extends TestCase {
    Integer[] integers1 = {1,2,3,4,5,6,7,8};
    Integer[] integers2 = {8,7,6,5,4,3,2,1};
    Integer[] integers3 = {1,2,3,4,5,6,7,8};
    Integer[] integers4 = {1,2,3,4,5,6,7,8};
    String[] strings5 = {"谭鑫","20172305", "网络空间安全系", "信息管理与信息系统"};
    String[] strings6 = {"谭鑫","YC404", "网络空间安全系", "信息管理与信息系统"};
    Character[] characters7 = {'a','d','8',',','T',';'};
    Character[] characters8 = {'m','f',';','8',',','X'};
    Double[] double9 = {2.3,5.0,4.87,1.22,8.645};
    Double[] double10 = {2.7,5.7,1.22,0.4,0.22};
    @Test
    //正序
    public void testSeraching1()
    {
        assertEquals("1 2 3 4 5 6 8 7", Sorting.selectionSort(integers1));//正常情况
    }
    @Test
    public void testSeraching2()
    {
        assertEquals("1 2 3 4 5 6 7 8", Sorting.selectionSort(integers2));//正常情况
    }
    @Test
    public void testSeraching3()
    {
        assertEquals("1 2 3 4 5 6 8 7", Sorting.selectionSort(integers3));//异常情况
    }
    @Test
    public void testSeraching4()
    {
        assertEquals("1 2 3 4 5 6 8 7", Sorting.selectionSort(integers4));//异常情况
    }
    @Test
    public void testSeraching5()
    {
        assertEquals("20172305 信息管理与信息系统 网络空间安全系 谭鑫", Sorting.selectionSort(strings5));//正常情况
    }
    @Test
    public void testSeraching6()
    {
        assertEquals("谭鑫 YC404 信息管理与信息系统 网络空间安全系", Sorting.selectionSort(strings6));//异常情况
    }
    @Test
    public void testSeraching7()
    {
        assertEquals(", 8 ; T a d", Sorting.selectionSort(characters7));//正常情况
    }
    @Test
    public void testSeraching8()
    {
        assertEquals(", 8 ; X d a", Sorting.selectionSort(characters8));//异常情况
    }
    @Test
    public void testSeraching9()
    {
        assertEquals("1.22 2.3 4.87 5.0 8.645", Sorting.selectionSort(double9));//正常情况
    }
    @Test
    public void testSeraching10()
    {
        assertEquals("1.22 2.3 4.87 5.0 8.645", Sorting.selectionSort(double10));//异常情况
    }
}